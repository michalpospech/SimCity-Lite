\documentclass{article}
\usepackage{hyperref}
\urlstyle{rm}
\usepackage{listings}
\usepackage{enumitem}
\usepackage{graphicx}

\usepackage{titling}
\usepackage[dvipsnames]{xcolor}
\usepackage{xspace}
\usepackage{minted}
\usepackage{caption}
\usepackage{subcaption}
\usepackage[margin=0.9in,footskip=0.2in]{geometry}
\usepackage[utf8]{inputenc}

\setlength{\parindent}{0em}
\setlength{\parskip}{1em}
\author{Michal Pospěch\\Charles University\\Faculty of Mathematics and Physics}
\title{SimCity Lite\\ \Large Programmer documentation}
\begin{document}
	\maketitle
	\thispagestyle{empty}
	\newpage
	\setcounter{page}{1}
	\pagenumbering{Roman}
	\tableofcontents
	\newpage
	\setcounter{page}{0}
	\pagenumbering{arabic}
	\section{Specification}
		The application serves as a event-based traffic simulator with traffic situation visualisation as specified in the original specification (available \href{https://drive.google.com/open?id=1lLQrQzD1a2z2aVvv3ZnLBUOH8Goo99GOPZ8-IzV_fDM}{here}). The main idea behind the principle of simulation is taken from \cite{burghout2006discrete}. The whole backend part of this project was developed with modularity in mind therefore any module could be replaced with a different implementation of it and it should work. Only the presentation layer (WinForms app) is designed specifically for this configuration.
		\subsection{Functional requirements}
		\begin{enumerate}
			\item Creation and modification of maps using GUI
			\item Simulation of traffic
			\item Generation of not completely unrealistic schedules for agents
			\item Visualisation of traffic situation (traffic density)
		\end{enumerate}
	\newpage
	\section{Architecture}
		The whole project is split into four smaller modules:
		\begin{enumerate}
			\item SimCity--Lite - Presentation layer, communicates with backend, provides visualisation (Functional requirement no.~1)
			\item Core - Provides interfaces implemented in other modules, contains event calendar essential for functionality and provides methods for visualisation (Functional requirement no.~4)
			\item Scheduler - Implents \mintinline{csharp}{IScheduler} interface and its task is to generate agents (Functional requirement no.~3)
			\item Enviroment - Implents \mintinline{csharp}{IEnviroment} interface and it contains all the logic of simulation (Functional requirement no.~2)
		\end{enumerate}
		\subsection{Core}
			This module is essential for the functionality of simulation for it contains definitions of all interfaces, methods for drawing the enviroment and en event calendar. The implementations of other modules get injected into it in the constructor.
			\subsubsection{Event aggregator}
				Event aggregator is one of the key funcionalities this module provides. Event aggregator is an object, that receives events from other objects. Upon reception of an event it notifies objects that have subscribed to this kind of events. In this application it is used only to add new events into the event calendar but it can be used by a logging module to log created events. \par
				It gets injected in the constructor, the interface a and used implementation are provided by Prism.Core NuGet package.
			\subsubsection{Event calendar}
				The other important functionality provided by this module is the event calendar. It stores the events that are left to be processed and processes them on request. \par
				Because the events must be processed in order, the event calendar must be a priority queue. Therefore it is implemented as a minimum-heap for it has $O(log(n))$ access to minimal element.
			\subsubsection{Drawing}
				The Core module also implements simple virtual methods for visualisation of nodes and links. \par
				Links are visualised as Bézier curves and its colour depends on the traffic situation in it. Nodes are visual as small circles and each type has its own colour:
				\begin{itemize}
					\item \textcolor{Cerulean}{Residential (light blue)}
					\item \textcolor{BurntOrange}{Industrial (orange)}
					\item \textcolor{BlueViolet}{Commercial (purple)}
					\item \textcolor{Gray}{Traffic (grey)}
				\end{itemize}
			\subsubsection{Interfaces}
				Interfaces defined in this module are important for the functionality of the simulation and they get injected in constructor. They need to meet some criteria of which not all can be expressed formally in code therefore they need to be described here.
				\begin{itemize}
					\item The implementation of \mintinline{csharp}{IEnviroment} must use the event aggregator to publish events, otherwise it's not going to work.
					\item The implementation of \mintinline{csharp}{IScheduler} must generate agents with fulfillable schedules.
				\end{itemize}
		\subsection{Scheduler}
			This module implements the \mintinline{csharp}{IScheduler} interface and is used to create agents with their schedules. The created agents must inherit from the \mintinline{csharp}{AgentBase} class defined in Core module.\par
			There are many possible ways how to implement the scheduler. Some are very complicated (such as the one described in \cite{charypar2005generating}) and some are very simple. The one used in this project is very simple and dumb, thus the name of the module "DumbScheduler".\par
			It is simple because everything depends on the factor of chance. The home, workplace and entertainment place of each agent is chosen randomly, there is factor of chance in duration of events, etc. Because every generated activity needs to have an approximate starting and ending time the duration of travel between nodes is calculated using \href{https://en.wikipedia.org/wiki/Floyd–Warshall_algorithm}{Floyd--Warshall's algorithm} and multiplied by a constant to account for possible slowdown by traffic situation. \par
			Before scheduling a few things must be checked beforehand:
				\begin{enumerate}
					\item Whether the network is \href{https://en.wikipedia.org/wiki/Strongly_connected_component}{strongly connected} - checked using algorithm based on \href{https://en.wikipedia.org/wiki/Kosaraju's_algorithm}{Kosaraju's algorithm}
					\item Whether there are any residential nodes
					\item Whether the capacity of commercial and industrial nodes is higher than capacity of residential nodes
				\end{enumerate}
		\subsection{Enviroment}
			This module implements the \mintinline{csharp}{IEnviroment} interface and provides implementations of \mintinline{csharp}{LinkBase}, \mintinline{csharp}{NodeBase} and \mintinline{csharp}{EventBase} classes from Core module. Its main part is to contain the simulation itself. The logic of simulation is heavily based on MEZZO, a mesoscopic event-based simulator described in \cite{burghout2006discrete}.
			\subsubsection{Nodes}
				There are 2 kinds of nodes, normal nodes (traffic) without capacity and container nodes that have assigned capacity (residential, commercial, industrial). Although they do not have it assigned directly but they have reference to a \mintinline{csharp}{Settings} object which stores it. This has an advantage that it could be chaged by user (it is not implemented in this application) after creation.
			\subsubsection{Links}
				Links are implementation of \mintinline{csharp}{LinkBase} class and they connect nodes. Because they are used as transfer between nodes, they need to have assigned various parameters (in bigger detail described in \cite{burghout2006discrete}). But they do not contain them directly but they reference a \mintinline{csharp}{Settings} object and get them from it. The advantage is the same as it is in the case of nodes. \par
				Because the agents must transfer between links, there needs to be implemented a mechanic to do so. This is provided by turning servers which control and limit the transfer of agents between links. \par
				To provide the functionality of variable traffic situations, the links are split into two parts: the running part and queue.
			\subsubsection{Events}
				Events are the element of simulation that contain most of the logic and are the key component of whole application. They all have a reference to event aggregator because they need to publish new events. There are 5 types of them:
				\begin{itemize}
					\item NodeInput - Handles inptut into nodes. Is scheduled after arrival of an agent to a neighbouring queue
					\item NodeOutput - Handles output from nodes, pathfinding to next target (\href{https://en.wikipedia.org/wiki/A*_search_algorithm}{A* algorithm}) and putting agent into correct node (it does bypass the capacity rule of links). Is scheduled after input of an agent to the node.
					\item TurningServerTrigger - Tries to transfer agent between two links if it is not blocked and there is an agent to transfer. Is scheduled by other turning servers, arrivals at queue and its unblocking.
					\item TuriningServerUnblock - Unblocks turning server and schedules its trigger.
					\item ArrivalAtQueue - Handles transfer of agents between moving part of a link and queue. Is scheduled by turning servers and output from nodes. 
				\end{itemize}


	\newpage
	\bibliographystyle{IEEEtran}
	\bibliography{references}
	\addcontentsline{toc}{section}{References}
\end{document}