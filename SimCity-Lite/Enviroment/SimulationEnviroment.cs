﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using Enviroment.Exceptions;
using Enviroment.SimulationEvents;
using Prism.Events;

namespace Enviroment
{
    /// <summary>
    /// Class that implements the IEnviroment interface and contians all information necessary for the simulation
    /// </summary>
    public class SimulationEnviroment : IEnviroment

    {
        /// <summary>
        /// Object used to create nodes
        /// </summary>
        private readonly NodeFactory _nodeFactory;

        /// <summary>
        /// Object used to create links
        /// </summary>
        private readonly LinkFactory _linkFactory;

        /// <summary>
        /// List of all links
        /// </summary>
        private readonly List<LinkBase> _links;

        /// <summary>
        /// List of all nodes
        /// </summary>
        private readonly List<NodeBase> _nodes;

        /// <summary>
        /// Settings of the simulation
        /// </summary>
        private readonly Settings _settings;

        /// <summary>
        /// Creates new instance of this class with provided settings
        /// </summary>
        /// <param name="settings">Settings</param>
        public SimulationEnviroment(Settings settings)
        {
            _settings = settings;
            _nodeFactory = new NodeFactory(settings);
            _linkFactory = new LinkFactory(settings);
            _links = new List<LinkBase>();
            _nodes = new List<NodeBase>();
        }

        /// <summary>
        /// dds ne node with specified parameters
        /// </summary>
        /// <param name="type">Type</param>
        /// <param name="positionX">X position</param>
        /// <param name="positionY">Y position</param>
        /// <param name="size">Size</param>
        /// <returns>Added node</returns>
        public Node AddNode(NodeType type, int positionX, int positionY, NodeSize? size = null)
        {
            if (GetNode(positionX, positionY) != null)
            {
                throw new DuplicitNodeException();
            }
            Node newNode = _nodeFactory.CreateNode(positionX, positionY, type, size);
            _nodes.Add(newNode);
            return newNode;
        }

        /// <summary>
        /// Gets node at position if it exists
        /// </summary>
        /// <param name="positionX">X position</param>
        /// <param name="positionY">Y position</param>
        /// <returns>Node at position or null if there is none.</returns>
        public Node GetNode(int positionX, int positionY)
        {
            return (Node) _nodes.FirstOrDefault(n => n.PositionX == positionX && n.PositionY == positionY);
        }

        /// <summary>
        /// Adds two links between specified nodes
        /// </summary>
        /// <param name="node1">Node</param>
        /// <param name="node2">Node</param>
        /// <param name="size">Link size</param>
        public void AddDoubleLink(NodeBase node1, NodeBase node2, LinkSize size)
        {
            if (_links.Count(l => l.End.Equals(node2) && l.Start.Equals(node1)) > 0)
            {
                throw new DuplicitLinkException();
            }
            Link newLink = _linkFactory.CreateLink(node1, node2, size);
            _links.Add(newLink);
            node1.AddOutcomingLink(newLink);
            node2.AddIncomingLink(newLink);
            newLink = _linkFactory.CreateLink(node2, node1, size);
            _links.Add(newLink);
            node1.AddIncomingLink(newLink);
            node2.AddOutcomingLink(newLink);
        }

        /// <summary>
        /// Gets list of all nodes
        /// </summary>
        public List<NodeBase> Nodes
        {
            get { return _nodes; }
        }

        /// <summary>
        /// Gets list of all link
        /// </summary>
        public List<LinkBase> Links
        {
            get { return _links; }
        }

        /// <summary>
        /// Removes specified node
        /// </summary>
        /// <param name="node">Node</param>
        public void RemoveNode(Node node)
        {
            foreach (Link link in node.OutcomingLinks)
            {
                _links.Remove(link);
                Node endNode = (Node) link.End;
                endNode.TurningServers.RemoveAll(ts => ts.Output.End.Equals(node) || ts.Input.Start.Equals(node));
                endNode.RemoveIncomingLink(endNode.IncomingLinks.First(l => l.Start.Equals(node)));
                var outcomingLink = endNode.OutcomingLinks.First(l => l.End.Equals(node));
                _links.Remove(outcomingLink);
                endNode.RemoveOutcomingLink(outcomingLink);
            }

            _nodes.Remove(node);
        }

        /// <summary>
        /// Updates specified node with new settings (by removing and adding it again)
        /// </summary>
        /// <param name="node">Node</param>
        /// <param name="newType">New node type</param>
        /// <param name="newSize">Ne node size</param>
        public void UpdateNode(Node node, NodeType newType, NodeSize? newSize)
        {
            List<NodeLinkPair> oldLinks = new List<NodeLinkPair>();
            foreach (Link link in node.OutcomingLinks)
            {
                oldLinks.Add(new NodeLinkPair((Node) link.End, link.Size));
            }
            RemoveNode(node);
            Node newNode = AddNode(newType, node.PositionX, node.PositionY, newSize);
            foreach (var oldLink in oldLinks)
            {
                AddDoubleLink(newNode, oldLink.Node, oldLink.Size);
            }
        }

        /// <summary>
        /// Gets settings of the simulation
        /// </summary>
        public Settings Settings
        {
            get { return _settings; }
        }

        /// <summary>
        /// Gets total capacity of nodes of given type
        /// </summary>
        /// <param name="type">Node type</param>
        /// <returns>Total capacity</returns>
        public int GetCapacity(NodeType type)
        {
            int result = _nodes.Where(n => n.Type == type).Cast<ContainerNode>().Sum(n => n.Capacity);
            return result;
        }

        /// <summary>
        /// Gets total count of agents in nodes of given type
        /// </summary>
        /// <param name="type">Node type</param>
        /// <returns>Total count of agents</returns>
        public int GetCount(NodeType type)
        {
            int result = _nodes.Where(n => n.Type == type).Cast<ContainerNode>().Sum(n => n.Count);
            return result;
        }

        /// <summary>
        /// Initailises the simulation with new agents with schedules
        /// </summary>
        /// <param name="agents">New agents</param>
        /// <param name="eventAggregator">Event aggregator used in simulation</param>
        /// <param name="initTime">Starting time</param>
        public void Initialise(List<AgentBase> agents, IEventAggregator eventAggregator, DateTime initTime)
        {
            foreach (var agent in agents)
            {
                var target = (ContainerNode) agent.Schedule.GetCurrentScheduleItem().Target;
                target.AddAgent(agent);
                var time = (DateTime) agent.Schedule.GetCurrentScheduleItem().End;
                var newEvent = new NodeOutput(time, eventAggregator, target, agent);
                var newEventArgs = new SimulationEventCreatedArgs(initTime, newEvent);
                eventAggregator.GetEvent<SimulationEventCreated>().Publish(newEventArgs);
                agent.Schedule.MoveSchedule();
            }
        }
    }
}