﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using Core;

namespace Enviroment
{
    /// <summary>
    /// Implementation of links that considers traffic density
    /// </summary>
    public class Link : LinkBase
    {
        /// <summary>
        /// Reference to settings
        /// </summary>
        private Settings _settings;

        /// <summary>
        /// Stores the agents that are in the queue. It stores them in the order of arrival.
        /// </summary>
        private List<AgentBase> _queue;

        /// <summary>
        /// Stores agents that are in the moving part of the links, they're not ordered
        /// </summary>
        private HashSet<AgentBase> _running;

        /// <summary>
        /// Size of the link
        /// </summary>
        private LinkSize _size;

        /// <summary>
        /// Limits how many agents can be skipped when turning
        /// </summary>
        private const int TurnLimit = 5;

        /// <summary>
        /// Creates new instance of this class with specified parameters
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="start">Start node</param>
        /// <param name="end">End node</param>
        /// <param name="settings">Reference to settings</param>
        /// <param name="size">Size</param>
        public Link(int id, NodeBase start, NodeBase end, Settings settings, LinkSize size) : base(start,
            end, id)
        {
            _settings = settings;
            _queue = new List<AgentBase>();
            _running = new HashSet<AgentBase>();
            _size = size;
            foreach (var incomingLink in ((Node) start).IncomingLinks.Cast<Link>())
            {
                if (!incomingLink.Start.Equals(end))
                {
                    ((Node) start).TurningServers.Add(new TurningServer(incomingLink, this));
                }
            }
            foreach (var outcomingLink in ((Node) end).OutcomingLinks.Cast<Link>())
            {
                if (!outcomingLink.End.Equals(start))
                {
                    ((Node) end).TurningServers.Add(new TurningServer(this, outcomingLink));
                }
            }
        }

        /// <summary>
        /// Gets the running part of link
        /// </summary>
        public HashSet<AgentBase> Running
        {
            get { return _running; }
        }

        /// <summary>
        /// Gets the capacity of link
        /// </summary>
        public override int Capacity
        {
            get
            {
                var capacity = Settings.CapacityPerLenght * Length;
                return (int) Math.Ceiling(capacity);
            }
        }

        /// <summary>
        /// Gets the number of agents in this link
        /// </summary>

        public override int Count
        {
            get { return _queue.Count + _running.Count; }
        }

        /// <summary>
        /// Gets the queue part of link
        /// </summary>
        public List<AgentBase> Queue
        {
            get { return _queue; }
        }

        /// <summary>
        /// Moves agent from the running part to the queue part of link
        /// </summary>
        /// <param name="agent">Agent to move</param>
        public void MoveAgent(AgentBase agent)
        {
            _running.Remove(agent);
            _queue.Add(agent);
        }

        /// <summary>
        /// Gets all agents in link
        /// </summary>
        public override List<AgentBase> PresentAgents
        {
            get
            {
                var list = new List<AgentBase>(_running);
                list.AddRange(_queue);
                return list;
            }
        }

        /// <summary>
        /// Empties out the link
        /// </summary>
        public override void Empty()
        {
            _queue = new List<AgentBase>();
            _running = new HashSet<AgentBase>();
        }

        /// <summary>
        /// Adds agent to the running part of link
        /// </summary>
        /// <param name="agent">Agent to add</param>
        public override void AddAgent(AgentBase agent)
        {
            _running.Add(agent);
        }

        /// <summary>
        /// Gets the LinkSettings that correspond to this size of link
        /// </summary>
        private LinkSettings Settings
        {
            get { return _settings.GetLinkSettings(Size); }
        }

        /// <summary>
        /// Gets travel time unaffected by traffic situation
        /// </summary>
        public override double BaseTravelTime
        {
            get

            {
                var speed = Settings.MaxSpeed;
                var time = Length / speed;
                return time;
            }
        }

        /// <summary>
        /// Gets travel time modified by traffic situation
        /// </summary>
        public double TravelTime
        {
            get
            {
                double speed;
                double density = _running.Count / (double) (Capacity - _queue.Count);
                if (density < Settings.MinDensity)
                {
                    speed = Settings.MaxSpeed;
                }
                else if (density > Settings.MaxDensity || Full)
                {
                    speed = Settings.MinSpeed;
                }
                else
                {
                    double densityCoeff = Math.Pow(
                        (density - Settings.MinDensity) / (Settings.MaxSpeed - Settings.MinDensity),
                        _settings.SimulationParameter2);
                    double speedCoeff = Math.Pow(1 - densityCoeff, _settings.SimulationParameter1);
                    speed = Settings.MinSpeed + (Settings.MaxSpeed - Settings.MinSpeed) * speedCoeff;
                }
                double time = Length / speed;
                return time;
            }
        }

        /// <summary>
        /// Gets whether the link is full or not
        /// </summary>
        public bool Full
        {
            get { return Count >= Capacity; }
        }

        /// <summary>
        /// Gets the size of link
        /// </summary>
        public LinkSize Size
        {
            get { return _size; }
        }

        /// <summary>
        /// Tries to get first agent in queue that wants to get to the specified link. Maximum index of the agent is modified by TurnLimit constant
        /// </summary>
        /// <param name="nextLink">Link</param>
        /// <returns>Agent that meets the requirements, null otherwise</returns>
        public AgentBase TryGetAgent(Link nextLink)
        {
            AgentBase result = null;
            bool found = false;
            int index = 0;
            while (!found && index < _queue.Count && index < TurnLimit)
            {
                if (_queue[index].RemainingLinks > 0 && _queue[index].NextLink.Equals(nextLink))
                {
                    found = true;
                    result = _queue[index];
                    _queue.RemoveAt(index);
                }
                index++;
            }

            return result;
        }

        /// <summary>
        /// Tries to get first agent in queue that wants to get to the specified node. Maximum index of the agent is modified by TurnLimit constant.
        /// </summary>
        /// <param name="node">Node</param>
        /// <returns>Agent that meets the requirements, null otherwise.</returns>
        public AgentBase TryGetAgent(ContainerNode node)
        {
            AgentBase result = null;
            bool found = false;
            int index = 0;
            while (!found && index < _queue.Count && index < TurnLimit)
            {
                if (_queue[index].Schedule.GetCurrentScheduleItem().Target.Equals(node))
                {
                    found = true;
                    result = _queue[index];
                    _queue.RemoveAt(index);
                }
                index++;
            }
            return result;
        }

        /// <summary>
        /// Gets width of drawn link
        /// </summary>
        /// <returns>Width of representation of link</returns>
        private int GetWidth()
        {
            int width = 5;
            switch (_size)
            {
                case LinkSize.Large:
                    width = 7;
                    break;
                case LinkSize.Normal:
                    width = 5;
                    break;
                case LinkSize.Small:
                    width = 3;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            return width;
        }

        /// <summary>
        /// Draws link, overrides inherited method
        /// </summary>
        /// <param name="graphics">Graphics to draw on</param>
        public override void Draw(Graphics graphics)
        {
            var start = new Point(Start.PositionX, Start.PositionY);
            var end = new Point(End.PositionX, End.PositionY);
            var color = GetLinkColor();
            var offset = GetOffsetPoint();

            using (var pen = new Pen(color, GetWidth()))
            {
                graphics.DrawBezier(pen, start, offset, offset, end);
            }
        }
    }
}