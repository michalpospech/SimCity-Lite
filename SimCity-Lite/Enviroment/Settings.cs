﻿using System.Collections.Generic;

namespace Enviroment
{
    /// <summary>
    /// Stores all enviroment settings
    /// </summary>
    public class Settings
    {
        /// <summary>
        /// Parameter that affects the calculation of travel time on nodes
        /// </summary>
        private readonly double _simulationParameter1;

        /// <summary>
        /// Parameter that affects the calculation of travel time on nodes
        /// </summary>
        private readonly double _simulationParameter2;

        /// <summary>
        /// Dictionary storing settings for all sizes of links
        /// </summary>
        private Dictionary<LinkSize, LinkSettings> _linkSettings;

        /// <summary>
        /// Dictionary storing settings for all sizes of nodes
        /// </summary>
        private Dictionary<NodeSize, NodeSettings> _nodeSettings;
        /// <summary>
        /// Creates a new instance of this class with specified values
        /// </summary>
        /// <param name="simulationParameter1">Simulation parameter</param>
        /// <param name="simulationParameter2">Simulation parameter</param>
        /// <param name="linkSettings">Dictionary of settings of links</param>
        /// <param name="nodeSettings">Dictionary of settings of nodes</param>
        public Settings(double simulationParameter1, double simulationParameter2,
            Dictionary<LinkSize, LinkSettings> linkSettings, Dictionary<NodeSize, NodeSettings> nodeSettings)
        {
            _simulationParameter1 = simulationParameter1;
            _simulationParameter2 = simulationParameter2;
            _linkSettings = linkSettings;
            _nodeSettings = nodeSettings;
        }
        /// <summary>
        /// Gets first simulation parameter
        /// </summary>
        public double SimulationParameter1
        {
            get { return _simulationParameter1; }
        }
        /// <summary>
        /// Gets second simulation parameter
        /// </summary>
        public double SimulationParameter2
        {
            get { return _simulationParameter2; }
        }
        /// <summary>
        /// Gets settings of link of specified size
        /// </summary>
        /// <param name="size">Link size</param>
        /// <returns>Link settings</returns>
        public LinkSettings GetLinkSettings(LinkSize size)
        {
            return _linkSettings[size];
        }
        /// <summary>
        /// Gets settings of node of specified size
        /// </summary>
        /// <param name="size">Node size</param>
        /// <returns>Node settings</returns>
        public NodeSettings GetNodeSettings(NodeSize size)
        {
            return _nodeSettings[size];
        }
    }
}