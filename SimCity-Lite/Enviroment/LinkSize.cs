﻿namespace Enviroment
{
    /// <summary>
    /// Enum of all possible sizes of links
    /// </summary>
    public enum LinkSize
    {
        /// <summary>
        /// Large link, represents highways
        /// </summary>
        Large,
        /// <summary>
        /// Normal link, represents classic roads
        /// </summary>
        Normal,
        /// <summary>
        /// Small link, represents small roads 
        /// </summary>
        Small
    }
}