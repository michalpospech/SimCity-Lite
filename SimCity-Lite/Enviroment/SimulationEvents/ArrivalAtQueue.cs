﻿using System;
using Core;
using Prism.Events;

namespace Enviroment.SimulationEvents
{
    /// <summary>
    /// Event that is triggered when agent reaches queue. 
    /// </summary>
    public class ArrivalAtQueue : SimulationEventBase
    {
        /// <summary>
        /// Agent that arrived
        /// </summary>
        private AgentBase _agent;

        /// <summary>
        /// Link
        /// </summary>
        private Link _link;

        /// <summary>
        /// Creates new instance of this class with specified parameters
        /// </summary>
        /// <param name="processTime">Time to procces the event</param>
        /// <param name="eventAggregator">Event aggregator</param>
        /// <param name="link">Link</param>
        /// <param name="agent">Agent</param>
        public ArrivalAtQueue(DateTime processTime, IEventAggregator eventAggregator, Link link,
            AgentBase agent) : base(
            processTime, eventAggregator)
        {
            _link = link;
            _agent = agent;
        }

        /// <summary>
        /// Processes the event. Transfers agent to queue and triggeres node input and turning servers at the end of link.
        /// </summary>
        public override void Process()
        {
            _link.MoveAgent(_agent);
            _agent.SwitchNextLink();
            foreach (var server in ((Node) _link.End).TurningServers)
            {
                if (server.Input.Equals(_link))
                {
                    var newEvent = new TurningServerTrigger(ProcessTime, EventAggregator, server);
                    PropagateNewSimulationEvent(newEvent);
                }
            }
            var end = _link.End as ContainerNode;
            if (end != null)
            {
                var inputCheck = new NodeInput(ProcessTime, EventAggregator, end);
                PropagateNewSimulationEvent(inputCheck);
            }
        }
    }
}