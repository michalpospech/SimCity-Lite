﻿using System;
using Core;
using Prism.Events;

namespace Enviroment.SimulationEvents
{
    /// <summary>
    /// Event that handles unblocking a turning server.
    /// </summary>
    public class TurningServerUnblock : SimulationEventBase
    {
        /// <summary>
        /// Turning server to unblock
        /// </summary>
        private TurningServer _server;

        /// <summary>
        /// Creates a new instance of this class with specified parameters
        /// </summary>
        /// <param name="processTime">Process time</param>
        /// <param name="eventAggregator">Event aggregator</param>
        /// <param name="server">Turning server</param>
        public TurningServerUnblock(DateTime processTime, IEventAggregator eventAggregator,
            TurningServer server) : base(processTime,
            eventAggregator)
        {
            _server = server;
        }

        /// <summary>
        /// Unblocks the turning server and triggers it
        /// </summary>
        public override void Process()
        {
            _server.Unblock();
            var newEvent = new TurningServerTrigger(ProcessTime, EventAggregator, _server);
            PropagateNewSimulationEvent(newEvent);
        }
    }
}