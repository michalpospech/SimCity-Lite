﻿using System;
using System.Linq;
using Core;
using Prism.Events;

namespace Enviroment.SimulationEvents
{
    /// <summary>
    /// Event that is triggered by arrival of agent at queue.
    /// </summary>
    public class NodeInput : SimulationEventBase
    {
        /// <summary>
        /// Node to proceess
        /// </summary>
        private ContainerNode _node;

        /// <summary>
        /// Creates new instance of this class with specified parameters
        /// </summary>
        /// <param name="processTime">Time to process event at</param>
        /// <param name="eventAggregator">Event aggregator</param>
        /// <param name="node">Node to process</param>
        public NodeInput(DateTime processTime, IEventAggregator eventAggregator, ContainerNode node) : base(processTime,
            eventAggregator)
        {
            _node = node;
        }

        /// <summary>
        /// Tries to get agent from every input link and schedules output from the link
        /// </summary>
        public override void Process()
        {
            foreach (var link in _node.IncomingLinks.Cast<Link>())
            {
                var agent = link.TryGetAgent(_node);
                while (agent != null && _node.Count < _node.Capacity)
                {
                    _node.AddAgent(agent);

                    DateTime? originalEndTime = agent.Schedule.GetCurrentScheduleItem().End;
                    TimeSpan? minDuration = agent.Schedule.GetCurrentScheduleItem().MinDuration;
                    if (!(minDuration == null || originalEndTime == null))
                    {
                        DateTime end = ((DateTime) originalEndTime).CompareTo(ProcessTime.Add((TimeSpan) minDuration)) >
                                       0
                            ? (DateTime) originalEndTime
                            : ProcessTime.Add((TimeSpan) minDuration);
                        agent.Schedule.MoveSchedule();
                        var newEvent = new NodeOutput(end, EventAggregator, _node, agent);
                        PropagateNewSimulationEvent(newEvent);
                    }
                    agent = link.TryGetAgent(_node);
                }
            }
        }
    }
}