﻿using System;
using Core;
using Prism.Events;

namespace Enviroment.SimulationEvents
{
    /// <summary>
    /// Event that handles output from node.
    /// </summary>
    public class NodeOutput : SimulationEventBase
    {
        /// <summary>
        /// Agent to output
        /// </summary>
        private AgentBase _agent;

        /// <summary>
        /// Node to output from
        /// </summary>
        private ContainerNode _node;

        /// <summary>
        /// Creates new instance of this class with specified parameters.
        /// </summary>
        /// <param name="processTime">Process time</param>
        /// <param name="eventAggregator">Event aggregator</param>
        /// <param name="node">Node</param>
        /// <param name="agent">Agent</param>
        public NodeOutput(DateTime processTime, IEventAggregator eventAggregator, ContainerNode node,
            AgentBase agent) : base(processTime, eventAggregator)
        {
            _node = node;
            _agent = agent;
        }
        /// <summary>
        /// Removes agent from the node, finds path to its target, puts it into corresponding link and schedules its arrival to queue.
        /// </summary>
        public override void Process()
        {
            _node.RemoveAgent(_agent);
            Node target = (Node) _agent.Schedule.GetCurrentScheduleItem().Target;
            Pathfinder pf = new Pathfinder(_node, target);
            _agent.Route = pf.GetRoute();
            Link nextLink = (Link) _agent.NextLink;
            double time = nextLink.TravelTime;
            nextLink.AddAgent(_agent);
            ArrivalAtQueue newEvent = new ArrivalAtQueue(ProcessTime.AddSeconds(time), EventAggregator, nextLink,
                _agent);
            PropagateNewSimulationEvent(newEvent);
        }
    }
}