﻿using System;
using System.Linq;
using Core;
using Prism.Events;

namespace Enviroment.SimulationEvents
{
    /// <summary>
    /// Event that handles transfer of agents between links
    /// </summary>
    public class TurningServerTrigger : SimulationEventBase
    {
        /// <summary>
        /// Turning server
        /// </summary>
        private TurningServer _server;

        /// <summary>
        /// Creates new instance of this class with specified parameters
        /// </summary>
        /// <param name="processTime">Process time</param>
        /// <param name="eventAggregator">Event aggregator</param>
        /// <param name="server">Turning server</param>
        public TurningServerTrigger(DateTime processTime, IEventAggregator eventAggregator,
            TurningServer server) : base(processTime, eventAggregator)
        {
            _server = server;
        }

        /// <summary>
        /// If the server is unblocked, it tries to transfer agent between links. If it succeeds it schedules its arrival to queue and triggers other turning servers
        /// </summary>
        public override void Process()
        {
            if (_server.Blocked || _server.Output.Full)
            {
                return;
            }
            var agent = _server.Input.TryGetAgent(_server.Output);
            if (agent != null)
            {
                var node = _server.Node as ContainerNode;
                if (node != null)
                {
                    var inputCheck = new NodeInput(ProcessTime, EventAggregator, node);
                    PropagateNewSimulationEvent(inputCheck);
                }

                _server.Block();
                SimulationEventBase newEvent = new TurningServerUnblock(ProcessTime.AddSeconds(TurningServer.Cooldown),
                    EventAggregator,
                    _server);
                PropagateNewSimulationEvent(newEvent);
                var travelTime = _server.Output.TravelTime;
                newEvent = new ArrivalAtQueue(ProcessTime.AddSeconds(travelTime), EventAggregator, _server.Output,
                    agent);
                PropagateNewSimulationEvent(newEvent);
                foreach (var server in ((Node) _server.Input.Start).TurningServers.Where(
                    s => s.Output.Equals(_server.Input)))
                {
                    newEvent = new TurningServerTrigger(ProcessTime, EventAggregator, server);
                    PropagateNewSimulationEvent(newEvent);
                }
            }
        }
    }
}