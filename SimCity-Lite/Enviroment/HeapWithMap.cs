﻿using System.Collections.Generic;
using Core;

namespace Enviroment
{
    /// <summary>
    /// Minimal heap of PathFinder nodes with O(1) access to any element. Used in A* algorithm.
    /// </summary>
    class HeapWithMap : MinHeap<PathFinderNode>
    {
        /// <summary>
        /// Indices of nodes in the heap
        /// </summary>
        private readonly Dictionary<Node, int> _indices;

        public HeapWithMap()
        {
            _indices = new Dictionary<Node, int>();
        }
        /// <summary>
        /// Overriden switch method that also switches the indices in _indices dictionary
        /// </summary>
        /// <param name="index1">Index of element to switch</param>
        /// <param name="index2">Index of the other element</param>
        protected override void SwitchElements(int index1, int index2)
        {
            _indices[Elements[index1].Node] = index2;
            _indices[Elements[index2].Node] = index1;
            base.SwitchElements(index1, index2);
        }
        /// <summary>
        /// Overriden add method that also adds the node to the _indices dictionary
        /// </summary>
        /// <param name="element">Element to add</param>
        public override void AddElement(PathFinderNode element)
        {
            _indices.Add(element.Node, Size);
            base.AddElement(element);
        }
        /// <summary>
        /// Gets the minimal node from the heap and removes it.
        /// </summary>
        /// <returns>Minimal node</returns>
        public override PathFinderNode GetMin()
        {
            var result = base.GetMin();
            _indices.Remove(result.Node);
            return result;
        }
        /// <summary>
        /// Method that checks whether the heap contains specified node
        /// </summary>
        /// <param name="node">Node</param>
        /// <returns>Result of the check</returns>
        public bool Contains(Node node)
        {
            return _indices.ContainsKey(node);
        }
        /// <summary>
        /// Tries to update the pathfinder node with new data 
        /// </summary>
        /// <param name="node">Node to update</param>
        /// <param name="link">New previous link</param>
        /// <param name="distance">New distance</param>
        public void UpdateNode(Node node, Link link, double distance)
        {
            int index = _indices[node];
            Elements[index].Update(link, distance);
            FixUp(index);
        }
    }
}