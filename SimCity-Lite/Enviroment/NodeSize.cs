﻿namespace Enviroment
{
    /// <summary>
    /// Enum for representing various sizes of nodes
    /// </summary>
    public enum NodeSize
    {
        /// <summary>
        /// Small
        /// </summary>
        Small,

        /// <summary>
        /// Normal
        /// </summary>
        Normal,

        /// <summary>
        /// Large
        /// </summary>
        Large
    }
}