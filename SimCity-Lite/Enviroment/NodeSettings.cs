﻿namespace Enviroment
{
    /// <summary>
    /// Class that contains setting for one size of node
    /// </summary>
    public class NodeSettings
    {
        /// <summary>
        /// Capacity of the node
        /// </summary>
        private int _capacity;

        /// <summary>
        /// Creates new instance of this class with set value
        /// </summary>
        /// <param name="capacity">Capacity</param>
        public NodeSettings(int capacity)
        {
            _capacity = capacity;
        }

        /// <summary>
        /// Getter for _capacity field
        /// </summary>
        public int Capacity
        {
            get { return _capacity; }
        }
    }
}