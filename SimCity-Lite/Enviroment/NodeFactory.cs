﻿using System;
using Core;

namespace Enviroment
{
    /// <summary>
    /// Class that creates all nodes and ensures uniqueness of ids
    /// </summary>
    public class NodeFactory

    {
        /// <summary>
        /// Number of created nodes
        /// </summary>
        private int _created;

        /// <summary>
        /// Reference to settings that gets injected into the created node
        /// </summary>
        private readonly Settings _settings;
        /// <summary>
        /// Creates new instance of this class
        /// </summary>
        /// <param name="settings">Settings</param>
        public NodeFactory(Settings settings)
        {
            _settings = settings;
            _created = 0;
        }

        /// <summary>
        /// Creates node with specified parameters
        /// </summary>
        /// <param name="positionX">X position</param>
        /// <param name="positionY">Y positions</param>
        /// <param name="type">Type of node</param>
        /// <param name="size">Size of node</param>
        /// <returns>New node</returns>
        public Node CreateNode(int positionX, int positionY, NodeType type, NodeSize? size = null)
        {
            int newId = ++_created;
            if (type != NodeType.Traffic)
            {
                if (size == null)
                {
                    throw new ArgumentNullException(nameof(size), "Size must be specified for this kind of node");
                }
                return new ContainerNode(newId, type, positionX, positionY, (NodeSize) size, _settings);
            }
            return new Node(newId, positionX, positionY, type);
        }
    }
}