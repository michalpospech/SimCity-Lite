﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Core;

namespace Enviroment
{
    /// <summary>
    /// Implementation of Nodes
    /// </summary>
    public class Node : NodeBase
    {
        /// <summary>
        /// List of links ending in this node
        /// </summary>
        private List<Link> _incoming;

        /// <summary>
        /// List of nodes starting in this node
        /// </summary>
        private List<Link> _outcoming;

        /// <summary>
        /// List of turning servers contained within this node
        /// </summary>
        private List<TurningServer> _turningServers;
        /// <summary>
        /// Creates new instance of this class with specified parameters
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="positionX">X position</param>
        /// <param name="positionY">Y position</param>
        /// <param name="type">Node type</param>
        public Node(int id, int positionX, int positionY, NodeType type) : base(id, type, positionX, positionY)
        {
            _turningServers = new List<TurningServer>();
            _incoming = new List<Link>();
            _outcoming = new List<Link>();
        }

        /// <summary>
        /// Gets list of turning servers within this node
        /// </summary>
        public List<TurningServer> TurningServers
        {
            get { return _turningServers; }
        }

        /// <summary>
        /// Gets list of links ending in this node cast to LinkBase
        /// </summary>
        public override List<LinkBase> IncomingLinks
        {
            get { return _incoming.Cast<LinkBase>().ToList(); }
        }

        /// <summary>
        /// Gets list of links starting in this node cast to LinkBase
        /// </summary>
        public override List<LinkBase> OutcomingLinks
        {
            get { return _outcoming.Cast<LinkBase>().ToList(); }
        }

        /// <summary>
        /// Adds new link ending in this node
        /// </summary>
        /// <param name="link">Link to add</param>
        public override void AddIncomingLink(LinkBase link)
        {
            _incoming.Add((Link) link);
        }

        /// <summary>
        /// Removes link ending in this node
        /// </summary>
        /// <param name="link">Link to remove</param>
        public override void RemoveIncomingLink(LinkBase link)
        {
            _incoming.Remove((Link) link);
        }

        /// <summary>
        /// Removes link starting in this node
        /// </summary>
        /// <param name="link">Link to remove</param>
        public override void RemoveOutcomingLink(LinkBase link)
        {
            _outcoming.Remove((Link) link);
        }

        /// <summary>
        /// Adds new link ending in this node
        /// </summary>
        /// <param name="link">Link to add</param>
        public override void AddOutcomingLink(LinkBase link)
        {
            _outcoming.Add((Link) link);
        }

    }
}