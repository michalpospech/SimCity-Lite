﻿namespace Enviroment
{
    /// <summary>
    /// Class used to store a pair of node and link size used during deletion and re-creation of nodes
    /// </summary>
    internal class NodeLinkPair
    {
        private readonly Node _node;
        private readonly LinkSize _size;

        /// <summary>
        /// Creates new instance of this class with provided values
        /// </summary>
        /// <param name="node">Node</param>
        /// <param name="size">Link size</param>
        public NodeLinkPair(Node node, LinkSize size)
        {
            _node = node;
            _size = size;
        }
        /// <summary>
        /// Getter for _node field
        /// </summary>
        public Node Node
        {
            get { return _node; }
        }
        /// <summary>
        /// Getter for _size field
        /// </summary>
        public LinkSize Size
        {
            get { return _size; }
        }
    }
}