﻿using Core;

namespace Enviroment
{
    /// <summary>
    /// Class that creates links and ensures uniqueness fo their ids
    /// </summary>
    public class LinkFactory
    {
        /// <summary>
        /// Number of created links
        /// </summary>
        private int _created;

        /// <summary>
        /// Reference to settings that gets injected into the created links
        /// </summary>
        private Settings _settings;
        /// <summary>
        /// Creates new instance of this class
        /// </summary>
        /// <param name="settings">Settings</param>
        public LinkFactory(Settings settings)
        {
            _settings = settings;
            _created = 0;
        }
        /// <summary>
        /// Creates new link with specified parameters
        /// </summary>
        /// <param name="start">Start node</param>
        /// <param name="end">End node</param>
        /// <param name="size">Size</param>
        /// <returns>Created link</returns>
        public Link CreateLink(NodeBase start, NodeBase end, LinkSize size)
        {
            var newId = ++_created;
            return new Link(newId, start, end, _settings, size);
        }
    }
}