﻿using System;

namespace Enviroment
{
    /// <summary>
    /// Class that is used to store nodes with info for the A* algorithm
    /// </summary>
    internal class PathFinderNode : IComparable
    {
        /// <summary>
        /// Stores eucliedean distance to target node
        /// </summary>
        private double _heuristic;
        /// <summary>
        /// Stores graph distance from starting node
        /// </summary>
        private double _distance;
        /// <summary>
        /// Node
        /// </summary>
        private Node _node;
        /// <summary>
        /// Link that is used in current shortest path to this node
        /// </summary>
        private Link _incomingLink;
        /// <summary>
        /// Creates new instance of this class with set values
        /// </summary>
        /// <param name="heuristic">Euclidean distance from target</param>
        /// <param name="distance">Graph distance from start</param>
        /// <param name="node">Node</param>
        /// <param name="incomingLink">Link used in the shortest path</param>
        public PathFinderNode(double heuristic, double distance, Node node, Link incomingLink)
        {
            _heuristic = heuristic;
            _distance = distance;
            _node = node;
            _incomingLink = incomingLink;
        }
        /// <summary>
        /// Getter for value used by CompareTo method
        /// </summary>
        private double Key
        {
            get { return _heuristic + _distance; }
        }
        /// <summary>
        /// Getter for _distance field
        /// </summary>
        public double Distance
        {
            get { return _distance; }
        }
        /// <summary>
        /// Updates the information with new if the distance would get lower
        /// </summary>
        /// <param name="link">New previous link</param>
        /// <param name="distance">New graph distance</param>
        public void Update(Link link, double distance)
        {
            if (distance < _distance)
            {
                _distance = distance;
                _incomingLink = link;
            }
        }
        /// <summary>
        /// Getter for _node
        /// </summary>
        public Node Node
        {
            get { return _node; }
        }
        /// <summary>
        /// Gettter for _incomingLink
        /// </summary>
        public Link IncomingLink
        {
            get { return _incomingLink; }
        }
        /// <summary>
        /// Overriden CompareTo method, compares Key properties
        /// </summary>
        /// <param name="obj">Object to compare to</param>
        /// <returns>Result of comparism</returns>
        public int CompareTo(object obj)
        {
            var other = obj as PathFinderNode;
            if (other != null)
            {
                return Key.CompareTo(other.Key);
            }
            throw new Exception();
        }
    }
}