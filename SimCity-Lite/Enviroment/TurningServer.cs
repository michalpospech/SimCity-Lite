﻿namespace Enviroment
{
    /// <summary>
    /// Class that controls transfer of agents between links
    /// </summary>
    public class TurningServer
    {
        /// <summary>
        /// Input link
        /// </summary>
        private Link _input;

        /// <summary>
        /// Output link
        /// </summary>
        private Link _output;

        /// <summary>
        /// Duration of the block after being used
        /// </summary>
        public const double Cooldown = 0.5;

        /// <summary>
        /// Stores whether the server is blocked or not
        /// </summary>
        private bool _blocked;
        /// <summary>
        /// Creates new instance of this class with specified input and output
        /// </summary>
        /// <param name="input">Input link</param>
        /// <param name="output">Output link</param>
        public TurningServer(Link input, Link output)
        {
            _input = input;
            _output = output;
            _blocked = false;
        }

        /// <summary>
        /// Gets information whether the turning server is blocked or not
        /// </summary>
        public bool Blocked
        {
            get { return _blocked; }
        }

        /// <summary>
        /// Gets input link
        /// </summary>
        public Link Input
        {
            get { return _input; }
        }

        /// <summary>
        /// Gets output link
        /// </summary>
        public Link Output
        {
            get { return _output; }
        }

        /// <summary>
        /// Gets node that contains this turning server
        /// </summary>
        public Node Node
        {
            get { return (Node) _output.Start; }
        }

        /// <summary>
        /// Blocks turning server
        /// </summary>
        public void Block()
        {
            _blocked = true;
        }

        /// <summary>
        /// Unblocks turning server
        /// </summary>
        public void Unblock()
        {
            _blocked = false;
        }
    }
}