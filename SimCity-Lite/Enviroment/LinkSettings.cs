﻿namespace Enviroment
{
    /// <summary>
    /// Class that stores settings for one size of links
    /// </summary>
    public class LinkSettings
    {
        /// <summary>
        /// Capacity per unit of length
        /// </summary>
        private readonly double _capacityPerLenght;
        /// <summary>
        /// Threshold for application of maximal speed
        /// </summary>
        private readonly double _minDensity;
        /// <summary>
        /// Threshold for application of minimal speed
        /// </summary>
        private readonly double _maxDensity;
        /// <summary>
        /// Maximal speed in units/sec
        /// </summary>
        private readonly double _maxSpeed;
        /// <summary>
        /// Minimal speed in nunits/sec
        /// </summary>
        private readonly double _minSpeed;
        /// <summary>
        /// Creates new instance of this class with specified values
        /// </summary>
        /// <param name="capacityPerLenght">Capacity in agents per unit of length</param>
        /// <param name="maxSpeed">Maximal speed in units per second</param>
        /// <param name="minSpeed">Minimal speed in units per second</param>
        /// <param name="maxDensity">Maximal traffic density (works as threshold)</param>
        /// <param name="minDensity">Minimal traffic density (works as threshold)</param>
        public LinkSettings(double capacityPerLenght, double maxSpeed, double minSpeed, double maxDensity,
            double minDensity)
        {
            _capacityPerLenght = capacityPerLenght;
            _maxSpeed = maxSpeed;
            _minSpeed = minSpeed;
            _maxDensity = maxDensity;
            _minDensity = minDensity;
        }
        /// <summary>
        /// Gets capacity per unit of lenght 
        /// </summary>
        public double CapacityPerLenght
        {
            get { return _capacityPerLenght; }
        }

        /// <summary>
        /// Gets maximal speed
        /// </summary>
        public double MaxSpeed
        {
            get { return _maxSpeed; }
        }
        /// <summary>
        /// Gets minimal speed
        /// </summary>
        public double MinSpeed
        {
            get { return _minSpeed; }
        }
        /// <summary>
        /// Gets minimal density of traffic
        /// </summary>
        public double MinDensity
        {
            get { return _minDensity; }
        }
        /// <summary>
        /// Gets maximal density of traffic
        /// </summary>
        public double MaxDensity
        {
            get { return _maxDensity; }
        }
    }
}