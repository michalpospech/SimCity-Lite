﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core;

namespace Enviroment
{
    /// <summary>
    /// Class that finds the shortest path between 2 nodes given current traffic situation
    /// </summary>
    class Pathfinder
    {
        private Node _start;
        private Node _end;
        /// <summary>
        /// Creates a new instance of this class with specified values
        /// </summary>
        /// <param name="start">Starting node</param>
        /// <param name="end">Target node</param>
        public Pathfinder(Node start, Node end)
        {
            _start = start;
            _end = end;
        }

        /// <summary>
        /// Implementation o A* algorithm, expects, that there is a path
        /// </summary>
        /// <returns>Shortes path between two nodes</returns>
        public Route GetRoute()
        {
            HeapWithMap heap = new HeapWithMap();
            Dictionary<Node, Link> predecessors = new Dictionary<Node, Link>();
            PathFinderNode currentPathFinderNode =
                new PathFinderNode(GetEuclideanDistance(_start, _end), 0, _start, null);
            //This method expects that there is such route
            while (!currentPathFinderNode.Node.Equals(_end))
            {
                foreach (var link in currentPathFinderNode.Node.OutcomingLinks.Cast<Link>())
                {
                    double distance = currentPathFinderNode.Distance + link.TravelTime;
                    Node end = (Node) link.End;
                    if (!heap.Contains(end) && !predecessors.ContainsKey(end))
                    {
                        double heuristic = GetEuclideanDistance(currentPathFinderNode.Node, end);
                        PathFinderNode pathFinderNode = new PathFinderNode(heuristic, distance, end, link);
                        heap.AddElement(pathFinderNode);
                    }
                    else if (!predecessors.ContainsKey(end))
                    {
                        heap.UpdateNode(end, link, distance);
                    }
                }
                predecessors.Add(currentPathFinderNode.Node, currentPathFinderNode.IncomingLink);
                currentPathFinderNode = heap.GetMin();
            }
            LinkedList<LinkBase> path = new LinkedList<LinkBase>();
            path.AddFirst(currentPathFinderNode.IncomingLink);
            Node currentNode = (Node) currentPathFinderNode.IncomingLink.Start;
            while (predecessors[currentNode] != null)
            {
                Link link = predecessors[currentNode];
                path.AddFirst(link);
                currentNode = (Node) link.Start;
            }
            Route route = new Route(path);
            return route;
        }
        /// <summary>
        /// Method that gets distance between two nodes
        /// </summary>
        /// <param name="node1">Node</param>
        /// <param name="node2">Node</param>
        /// <returns>Euclidean distance between the two nodes</returns>
        private double GetEuclideanDistance(Node node1, Node node2)
        {
            double x = node1.PositionX - node2.PositionX;
            double y = node1.PositionY - node2.PositionY;
            double res = Math.Sqrt(x * x + y * y);
            return res;
        }
    }
}