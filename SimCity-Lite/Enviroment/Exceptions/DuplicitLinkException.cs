﻿using System;

namespace Enviroment.Exceptions
{
    /// <summary>
    /// Exception thrown when adding link that connects two already connected nodes
    /// </summary>
    public class DuplicitLinkException : Exception
    {
    }
}