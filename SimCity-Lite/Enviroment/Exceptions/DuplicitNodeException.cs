﻿using System;

namespace Enviroment.Exceptions
{
    /// <summary>
    /// Exception thrown when trying to add node to an occupied place
    /// </summary>
    public class DuplicitNodeException : Exception
    {
    }
}