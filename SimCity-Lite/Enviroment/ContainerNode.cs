﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Core;

namespace Enviroment
{
    /// <summary>
    /// Node that can has agents in it, used to implement the commercial, industrial and residential nodes
    /// </summary>
    public class ContainerNode : Node, IAgentContainer

    {
        /// <summary>
        /// Creates new node with specified parameters, position and id are passed to base constuctor
        /// </summary>
        /// <param name="id">Unique id</param>
        /// <param name="type">Node type</param>
        /// <param name="positionX">X coordinate</param>
        /// <param name="positionY">Y coordinate</param>
        /// <param name="size">Node size</param>
        /// <param name="settings">Reference to settings</param>
        public ContainerNode(int id, NodeType type, int positionX, int positionY, NodeSize size,
            Settings settings) : base(id, positionX, positionY, type)
        {
            _presentAgents = new HashSet<AgentBase>();
            _size = size;
            _settings = settings;
        }

        /// <summary>
        /// Stores present agents
        /// </summary>
        private HashSet<AgentBase> _presentAgents;

        /// <summary>
        /// Size of the node
        /// </summary>
        private readonly NodeSize _size;

        /// <summary>
        /// Reference to object that stores settings
        /// </summary>
        private readonly Settings _settings;

        /// <summary>
        /// Capacity of the node
        /// </summary>
        public int Capacity
        {
            get { return _settings.GetNodeSettings(_size).Capacity; }
        }

        /// <summary>
        /// Number of agents in the node
        /// </summary>
        public int Count
        {
            get { return _presentAgents.Count; }
        }

        /// <summary>
        /// Getter for agents currently in the node
        /// </summary>
        public List<AgentBase> PresentAgents
        {
            get { return new List<AgentBase>(_presentAgents); }
        }

        /// <summary>
        /// Empties out the node
        /// </summary>
        public void Empty()
        {
            _presentAgents = new HashSet<AgentBase>();
        }

        /// <summary>
        /// Size of the node
        /// </summary>
        public NodeSize Size
        {
            get { return _size; }
        }

        /// <summary>
        /// Adds agent to the node
        /// </summary>
        /// <param name="agent">Agent to add</param>
        public void AddAgent(AgentBase agent)
        {
            if (_presentAgents.Count == Capacity)
            {
                throw new Exception();
            }

            _presentAgents.Add(agent);
        }

        /// <summary>
        /// Removes agent from the node
        /// </summary>
        /// <param name="agent">Agent to remove</param>
        public void RemoveAgent(AgentBase agent)
        {
            _presentAgents.Remove(agent);
        }
        /// <summary>
        /// Gets draw radius of the node
        /// </summary>
        private int DrawRadius
        {
            get
            {
                int radius = 5;
                switch (_size)
                {
                    case NodeSize.Small:
                        radius = 5;
                        break;
                    case NodeSize.Normal:
                        radius = 7;
                        break;
                    case NodeSize.Large:
                        radius = 9;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                return radius;
            }
        }
        /// <summary>
        /// Draws node, overrides inherited method
        /// </summary>
        /// <param name="graphics">Graphics to use</param>
        public override void Draw(Graphics graphics)
        {
            Point coordinates = new Point(PositionX - DrawRadius, PositionY - DrawRadius);
            var boundingRectangle = new Rectangle(coordinates, new Size(DrawRadius * 2 + 1, DrawRadius * 2 + 1));
            using (var brush = new SolidBrush(DrawColour))
            {
                graphics.FillEllipse(brush,boundingRectangle);
            }
            using (var brush = new SolidBrush(Color.White))
            {
                using (var font = new Font("Arial", DrawRadius))
                {
                    var textFormat = new StringFormat();
                    textFormat.Alignment = StringAlignment.Center;
                    textFormat.LineAlignment = StringAlignment.Center;
                    graphics.DrawString(Count.ToString(), font, brush, boundingRectangle, textFormat);
                }
            }

        }
    }
}