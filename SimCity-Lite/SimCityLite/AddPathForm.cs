﻿using System;
using System.Linq;
using System.Windows.Forms;
using Enviroment;
using Enviroment.Exceptions;

namespace SimCity_Lite
{
    public partial class AddPathForm : Form
    {
        public AddPathForm(Node node1, Node node2, SimulationEnviroment enviroment, MainForm parent)
        {
            _node1 = node1;
            _node2 = node2;
            _enviroment = enviroment;
            _parent = parent;
            Closed += _parent.OnChildFormClose;
            InitializeComponent();
        }

        private Node _node1;
        private Node _node2;
        private SimulationEnviroment _enviroment;
        private MainForm _parent;

        private void _cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void _addPathButton_Click(object sender, EventArgs e)
        {
            LinkSize linkSize = (LinkSize) _pathSizeBox.Controls.OfType<RadioButton>()
                .FirstOrDefault(rb => rb.Checked)
                .Tag;
            try
            {
                _enviroment.AddDoubleLink(_node1, _node2, linkSize);
                _parent.MarkedNode = _node2;
            }
            catch (DuplicitLinkException)
            {
                MessageBox.Show("This link already exists");
            }
            finally
            {
                Close();
            }
        }
    }
}