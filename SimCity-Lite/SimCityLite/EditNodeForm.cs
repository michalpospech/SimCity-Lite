﻿using System;
using System.Linq;
using System.Windows.Forms;
using Core;
using Enviroment;

namespace SimCity_Lite
{
    public partial class EditNodeForm : Form
    {
        public EditNodeForm(Node node, MainForm parent, SimulationEnviroment enviroment)
        {
            _node = node;
            _enviroment = enviroment;
            InitializeComponent();
            Closed += parent.OnChildFormClose;
            _parent = parent;
            if (node.Type != NodeType.Traffic)
            {
                _nodeTypeBox.Controls.OfType<RadioButton>()
                    .FirstOrDefault(rb => (NodeType) rb.Tag == node.Type)
                    .Checked = true;
                _nodeSizeBox.Controls.OfType<RadioButton>()
                    .FirstOrDefault(rb => (NodeSize) rb.Tag == ((ContainerNode) node).Size)
                    .Checked = true;
            }
            else
            {
                _trafficNodeRadioButton.Checked = true;
                _normalNodeRadioButton.Checked = true;
                _nodeSizeBox.Enabled = false;
            }
        }

        private SimulationEnviroment _enviroment;
        private Node _node;
        private MainForm _parent;

        private void _trafficNodeRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            _nodeSizeBox.Enabled = !_nodeSizeBox.Enabled;
        }

        private void _editButton_Click(object sender, EventArgs e)
        {
            NodeType type = (NodeType)_nodeTypeBox.Controls.OfType<RadioButton>().FirstOrDefault(rb => rb.Checked).Tag;
            NodeSize? size = null;
            if (type != NodeType.Traffic)
            {
                size = (NodeSize?)_nodeSizeBox.Controls.OfType<RadioButton>().FirstOrDefault(rb => rb.Checked).Tag;
            }
            _enviroment.UpdateNode(_node, type,size);
            Close();
            
        }

        private void _removeButton_Click(object sender, EventArgs e)
        {
            _enviroment.RemoveNode(_node);
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _parent.MarkedNode = _node;
            Close();
        }

        private void _cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}