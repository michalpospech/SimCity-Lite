﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using Core;
using DumbScheduler;
using DumbScheduler.Exceptions;
using Enviroment;
using Prism.Events;

namespace SimCity_Lite
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            background = new Bitmap(1499, 1499);
            InitializeComponent();

            using (var g = Graphics.FromImage(background))
            {
                Brush white = new SolidBrush(Color.GhostWhite);
                g.FillRectangle(white, 0, 0, background.Width, background.Height);
                Pen grey = new Pen(Color.DimGray);
                for (int coord = 24; coord < background.Width; coord += 50)
                {
                    g.DrawLine(grey, coord, 0, coord, background.Height);
                    g.DrawLine(grey, 0, coord, background.Width, coord);
                }
                grey.Dispose();
                white.Dispose();
            }
            renderCenterX = background.Width / 2;
            renderCenterY = background.Height / 2;
            _eventAggregator = new EventAggregator();
            //Values are experimental
            Dictionary<NodeSize, NodeSettings> nodeSettings = new Dictionary<NodeSize, NodeSettings>
            {
                {NodeSize.Large, new NodeSettings(50)},
                {NodeSize.Normal, new NodeSettings(25)},
                {NodeSize.Small, new NodeSettings(10)}
            };
            Dictionary<LinkSize, LinkSettings> linkSettings = new Dictionary<LinkSize, LinkSettings>
            {
                {LinkSize.Large, new LinkSettings(0.2, 0.2, 0.05, 0.8, 0.3)},
                {LinkSize.Normal, new LinkSettings(0.1, 0.1, 0.03, 0.75, 0.35)},
                {LinkSize.Small, new LinkSettings(0.05, 0.05, 0.02, 0.6, 0.35)}
            };
            _simulationSettings = new Settings(1, 1, linkSettings, nodeSettings);
            _simulationEnviroment = new SimulationEnviroment(_simulationSettings);

            _engine = new Engine(_eventAggregator, _simulationEnviroment,
                new Scheduler(_simulationEnviroment));

            _shownTime = new DateTime(2017, 1, 1, 6, 0, 0);
            _capacities = new Dictionary<NodeType, int>()
            {
                {NodeType.Commercial, 0},
                {NodeType.Residential, 0},
                {NodeType.Industrial, 0}
            };
            _counts = new Dictionary<NodeType, int>()
            {
                {NodeType.Commercial, 0},
                {NodeType.Residential, 0},
                {NodeType.Industrial, 0}
            };
            _markerPen = new Pen(Color.Magenta, 3);
            UpdateInfo();
            _displayMap = (Bitmap) _map.Clone();
            Render();
            _oldCrossX = null;
            _oldCrossY = null;
            PrintInfo();
        }

        private void UpdateMap()
        {
            Bitmap oldMap = _map;

            _map = (Bitmap) background.Clone();
            if (oldMap != null)
            {
                oldMap.Dispose();
            }
            using (var g = Graphics.FromImage(_map))
            {
                _engine.Draw(g);
                if (_markedNode != null)
                {
                    int x = _markedNode.PositionX;
                    int y = _markedNode.PositionY;
                    Rectangle boundingRectangle = new Rectangle(x - 10, y - 10, 21, 21);
                    Pen pen = new Pen(Color.YellowGreen, 5);
                    g.DrawEllipse(pen, boundingRectangle);
                    pen.Dispose();
                }
            }
            var mouse = pictureBox.PointToClient(MousePosition);
            if (mouse.X > 0 && mouse.X < pictureBox.Width && mouse.Y > 0 && mouse.Y < pictureBox.Height)
            {
                MakeCross(mouse.X, mouse.Y);
            }
            else
            {
                _displayMap?.Dispose();
                _displayMap = (Bitmap) _map.Clone();
            }
        }

        private void Render()
        {
            int topLeftX = renderCenterX - pictureBox.Width / 2;
            int topLeftY = renderCenterY - pictureBox.Height / 2;
            Point topLeft = new Point(topLeftX, topLeftY);

            Rectangle viewBoundary = new Rectangle(topLeft, pictureBox.Size);
            viewBoundary.Y = viewBoundary.Bottom > _map.Height ? _map.Height - viewBoundary.Height : viewBoundary.Y;
            viewBoundary.Y = 0 > viewBoundary.Y ? 0 : viewBoundary.Y;
            viewBoundary.X = viewBoundary.Right > _map.Width ? _map.Width - viewBoundary.Width : viewBoundary.X;
            viewBoundary.X = 0 > viewBoundary.X ? 0 : viewBoundary.X;
            Bitmap view = _displayMap.Clone(viewBoundary, _displayMap.PixelFormat);
            Image oldImage = pictureBox.Image;
            pictureBox.Image = view;
            oldImage?.Dispose();
        }

        private int _oldMouseCoordX;
        private int _oldMouseCoordY;
        private EventAggregator _eventAggregator;
        private SimulationEnviroment _simulationEnviroment;
        private Settings _simulationSettings;
        private Bitmap _map;
        private Bitmap _displayMap;
        private bool _hasShownForm;
        private Node _markedNode;
        private Engine _engine;
        private DateTime _shownTime;
        private Dictionary<NodeType, int> _capacities;
        private Dictionary<NodeType, int> _counts;
        private Pen _markerPen;
        private int? _oldCrossX;
        private int? _oldCrossY;

        private void moveMap(object sender, MouseEventArgs e)
        {
            int deltaX = _oldMouseCoordX - e.X;
            int deltaY = _oldMouseCoordY - e.Y;
            _oldMouseCoordX = e.X;
            _oldMouseCoordY = e.Y;
            renderCenterX += deltaX;
            renderCenterY += deltaY;
            renderCenterX = renderCenterX > pictureBox.Width / 2 ? renderCenterX : pictureBox.Width / 2;
            renderCenterX = renderCenterX < background.Width - pictureBox.Width / 2
                ? renderCenterX
                : background.Width - pictureBox.Width / 2;
            renderCenterY = renderCenterY > pictureBox.Height / 2 ? renderCenterY : pictureBox.Height / 2;
            renderCenterY = renderCenterY < background.Height - pictureBox.Height / 2
                ? renderCenterY
                : background.Height - pictureBox.Height / 2;
            Render();
        }

        private void MoveMouse(object sender, MouseEventArgs e)
        {
            int x = renderCenterX - pictureBox.Width / 2 + e.X;
            int y = renderCenterY - pictureBox.Height / 2 + e.Y;

            if (MakeCross(x, y))
            {
                Render();
            }
        }

        private bool MakeCross(int x, int y)
        {
            int coordX = (x / 50) * 50 + 24;
            int coordY = (y / 50) * 50 + 24;
            if (coordX != _oldCrossX || coordY != _oldCrossY)
            {
                _oldCrossX = coordX;
                _oldCrossY = coordY;
                Bitmap newDisplayMap = (Bitmap) _map.Clone();
                using (var g = Graphics.FromImage(newDisplayMap))
                {
                    g.DrawLine(_markerPen, coordX - 7, coordY, coordX + 8, coordY);
                    g.DrawLine(_markerPen, coordX, coordY - 7, coordX, coordY + 8);
                }
                _displayMap?.Dispose();
                _displayMap = newDisplayMap;
                return true;
            }
            return false;
        }

        private void pictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                _oldMouseCoordX = e.X;
                _oldMouseCoordY = e.Y;
                Cursor.Current = Cursors.SizeAll;
                pictureBox.MouseMove += moveMap;
            }

            if (e.Button == MouseButtons.Left)
            {
                //Check for existence of a node and other stuff
                int x = renderCenterX - pictureBox.Width / 2 + e.X;
                int y = renderCenterY - pictureBox.Height / 2 + e.Y;
                int nodeCoordX = (x / 50) * 50 + 24;
                int nodeCoordY = (y / 50) * 50 + 24;
                Node node = _simulationEnviroment.GetNode(nodeCoordX, nodeCoordY);
                if (node == null && !_hasShownForm)
                {
                    var newForm = new AddNodeForm(_simulationEnviroment, nodeCoordX, nodeCoordY, this);
                    newForm.Shown += OnChildFormOpen;
                    newForm.Show();

                }
                if (node != null && OwnedForms.Length==0)
                {
                    if (_markedNode == null)
                    {
                        var newForm = new EditNodeForm(node, this, _simulationEnviroment);
                        newForm.Shown += OnChildFormOpen;
                        newForm.Show();

                    }
                    else
                    {
                        if (!node.Equals(_markedNode))
                        {
                            
                            var newForm = new AddPathForm(_markedNode, node, _simulationEnviroment, this);
                            newForm.Shown += OnChildFormOpen;
                            newForm.Show();
                        }
                        else
                        {
                            _markedNode = null;
                            UpdateInfo();
                            Render();
                        }
                    }
                }
            }
        }

        private void pictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBox.MouseMove -= moveMap;

            Cursor.Current = Cursors.Arrow;
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            Render();
        }

        public void OnChildFormClose(object sender, EventArgs args)
        {
            _hasShownForm = false;
            stepButton.Enabled = false;
            UpdateInfo();
            PrintInfo();
            Render();
        }

        public void OnChildFormOpen(object sender, EventArgs args)
        {
            _hasShownForm = true;
        }

        public Node MarkedNode
        {
            get { return _markedNode; }
            set { _markedNode = value; }
        }

        private void PrintInfo()
        {
            string text =
                "Info\n" +
                $"Time: {_shownTime.TimeOfDay}\n" +
                $"Residental: {_counts[NodeType.Residential]}/{_capacities[NodeType.Residential]}\n" +
                $"Industrial: {_counts[NodeType.Industrial]}/{_capacities[NodeType.Industrial]}\n" +
                $"Commercial: {_counts[NodeType.Commercial]}/{_capacities[NodeType.Commercial]}";
            _infoLabel.Text = text;
        }

        private void UpdateInfo()
        {
            UpdateMap();
            _counts[NodeType.Residential] = _simulationEnviroment.GetCount(NodeType.Residential);
            _counts[NodeType.Industrial] = _simulationEnviroment.GetCount(NodeType.Industrial);
            _counts[NodeType.Commercial] = _simulationEnviroment.GetCount(NodeType.Commercial);

            _capacities[NodeType.Commercial] = _simulationEnviroment.GetCapacity(NodeType.Commercial);
            _capacities[NodeType.Industrial] = _simulationEnviroment.GetCapacity(NodeType.Industrial);
            _capacities[NodeType.Residential] = _simulationEnviroment.GetCapacity(NodeType.Residential);
        }

        private void initialiseButton_Click(object sender, EventArgs e)
        {
            try
            {
                _engine.Start();
                _shownTime = _engine.CurrentTime;
                UpdateInfo();
                PrintInfo();
                Render();
                stepButton.Enabled = true;
            }
            catch (NotConnectedException)
            {
                MessageBox.Show("The provided network is not strongly connected, please add some links");
            }
            catch (InfrastructureException)
            {
                MessageBox.Show("There isn't enough commercial or industrial nodes, please add some");
            }
            catch (NoCapacityException)
            {
                MessageBox.Show("There are no residential nodes, please add some to enable simulation");
            }
        }

        private void stepButton_Click(object sender, EventArgs e)
        {
            while (_shownTime.Add(new TimeSpan(0, 10, 0)) > _engine.CurrentTime && _engine.EventCount > 0)
            {
                UpdateInfo();
                _engine.ProcessNext();
            }
            _shownTime = _shownTime.Add(new TimeSpan(0, 10, 0));
            Render();
            PrintInfo();
            if (_engine.EventCount == 0)
            {
                UpdateInfo();
                Render();
                PrintInfo();
                MessageBox.Show(
                    "There are no events left to process, please re-initialise the simulation to run it again");
                stepButton.Enabled = false;
            }
        }
    }
}