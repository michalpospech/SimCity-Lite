﻿using System;
using System.Linq;
using System.Windows.Forms;
using Core;
using Enviroment;

namespace SimCity_Lite
{
    public partial class AddNodeForm : Form
    {
        public AddNodeForm(SimulationEnviroment enviroment, int xCoord, int yCoord, MainForm parent)
        {
            _enviroment = enviroment;
            _xCoord = xCoord;
            _yCoord = yCoord;
            Closed += parent.OnChildFormClose;
            _parent = parent;
            InitializeComponent();
        }

        private int _xCoord;
        private int _yCoord;
        private SimulationEnviroment _enviroment;
        private MainForm _parent;

        private void _trafficNodeRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            _nodeSizeBox.Enabled = !_nodeSizeBox.Enabled;
        }

        private void _cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void _addButton_Click(object sender, EventArgs e)
        {
            NodeType type = (NodeType) _nodeTypeBox.Controls.OfType<RadioButton>().FirstOrDefault(rb => rb.Checked).Tag;
            NodeSize? size = null;
            if (type != NodeType.Traffic)
            {
                size = (NodeSize?) _nodeSizeBox.Controls.OfType<RadioButton>().FirstOrDefault(rb => rb.Checked).Tag;
            }

            Node newNode = _enviroment.AddNode(type, _xCoord, _yCoord, size);
           
            Close();
            if (_parent.MarkedNode != null)
            {
                var newForm = new AddPathForm(_parent.MarkedNode, newNode, _enviroment, _parent);
                newForm.Shown +=  _parent.OnChildFormOpen;
                newForm.Show();

            }
        }
    }
}