﻿using System.ComponentModel;
using System.Windows.Forms;
using Enviroment;

namespace SimCity_Lite
{
    partial class AddPathForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._pathSizeBox = new System.Windows.Forms.GroupBox();
            this._smallPathRadioButton = new System.Windows.Forms.RadioButton();
            this._normalPathRadioButton = new System.Windows.Forms.RadioButton();
            this._largePathRadioButton = new System.Windows.Forms.RadioButton();
            this._addPathButton = new System.Windows.Forms.Button();
            this._cancelButton = new System.Windows.Forms.Button();
            this._pathSizeBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // _pathSizeBox
            // 
            this._pathSizeBox.Controls.Add(this._smallPathRadioButton);
            this._pathSizeBox.Controls.Add(this._normalPathRadioButton);
            this._pathSizeBox.Controls.Add(this._largePathRadioButton);
            this._pathSizeBox.Location = new System.Drawing.Point(12, 12);
            this._pathSizeBox.Name = "_pathSizeBox";
            this._pathSizeBox.Size = new System.Drawing.Size(101, 93);
            this._pathSizeBox.TabIndex = 0;
            this._pathSizeBox.TabStop = false;
            this._pathSizeBox.Text = "Path Size";
            // 
            // _smallPathRadioButton
            // 
            this._smallPathRadioButton.AutoSize = true;
            this._smallPathRadioButton.Location = new System.Drawing.Point(6, 65);
            this._smallPathRadioButton.Name = "_smallPathRadioButton";
            this._smallPathRadioButton.Size = new System.Drawing.Size(50, 17);
            this._smallPathRadioButton.TabIndex = 2;
            this._smallPathRadioButton.TabStop = true;
            this._smallPathRadioButton.Text = "Small";
            this._smallPathRadioButton.UseVisualStyleBackColor = true;
            _smallPathRadioButton.Tag = LinkSize.Small;
            // 
            // _normalPathRadioButton
            // 
            this._normalPathRadioButton.AutoSize = true;
            this._normalPathRadioButton.Checked = true;
            this._normalPathRadioButton.Location = new System.Drawing.Point(6, 42);
            this._normalPathRadioButton.Name = "_normalPathRadioButton";
            this._normalPathRadioButton.Size = new System.Drawing.Size(58, 17);
            this._normalPathRadioButton.TabIndex = 1;
            this._normalPathRadioButton.TabStop = true;
            this._normalPathRadioButton.Tag = LinkSize.Normal;
            this._normalPathRadioButton.Text = "Normal";
            this._normalPathRadioButton.UseVisualStyleBackColor = true;
            // 
            // _largePathRadioButton
            // 
            this._largePathRadioButton.AutoSize = true;
            this._largePathRadioButton.Location = new System.Drawing.Point(6, 19);
            this._largePathRadioButton.Name = "_largePathRadioButton";
            this._largePathRadioButton.Size = new System.Drawing.Size(52, 17);
            this._largePathRadioButton.TabIndex = 0;
            this._largePathRadioButton.TabStop = true;
            this._largePathRadioButton.Tag = LinkSize.Large;
            this._largePathRadioButton.Text = "Large";
            this._largePathRadioButton.UseVisualStyleBackColor = true;
            // 
            // _addPathButton
            // 
            this._addPathButton.Location = new System.Drawing.Point(12, 111);
            this._addPathButton.Name = "_addPathButton";
            this._addPathButton.Size = new System.Drawing.Size(102, 23);
            this._addPathButton.TabIndex = 1;
            this._addPathButton.Text = "Add path";
            this._addPathButton.UseVisualStyleBackColor = true;
            this._addPathButton.Click += new System.EventHandler(this._addPathButton_Click);
            // 
            // _cancelButton
            // 
            this._cancelButton.Location = new System.Drawing.Point(12, 140);
            this._cancelButton.Name = "_cancelButton";
            this._cancelButton.Size = new System.Drawing.Size(101, 23);
            this._cancelButton.TabIndex = 2;
            this._cancelButton.Text = "Cancel";
            this._cancelButton.UseVisualStyleBackColor = true;
            this._cancelButton.Click += new System.EventHandler(this._cancelButton_Click);
            // 
            // AddPathForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(126, 168);
            this.Controls.Add(this._cancelButton);
            this.Controls.Add(this._addPathButton);
            this.Controls.Add(this._pathSizeBox);
            this.Name = "AddPathForm";
            this.Text = "Add Path";
            this._pathSizeBox.ResumeLayout(false);
            this._pathSizeBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private GroupBox _pathSizeBox;
        private RadioButton _smallPathRadioButton;
        private RadioButton _normalPathRadioButton;
        private RadioButton _largePathRadioButton;
        private Button _addPathButton;
        private Button _cancelButton;
    }
}