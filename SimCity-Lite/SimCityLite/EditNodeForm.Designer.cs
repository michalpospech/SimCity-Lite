﻿using System.ComponentModel;
using System.Windows.Forms;
using Core;
using Enviroment;

namespace SimCity_Lite
{
    partial class EditNodeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._nodeTypeBox = new System.Windows.Forms.GroupBox();
            this._nodeSizeBox = new System.Windows.Forms.GroupBox();
            this._editButton = new System.Windows.Forms.Button();
            this._cancelButton = new System.Windows.Forms.Button();
            this._removeButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this._smallNodeRadioButton = new System.Windows.Forms.RadioButton();
            this._normalNodeRadioButton = new System.Windows.Forms.RadioButton();
            this._largeNodeRadioButton = new System.Windows.Forms.RadioButton();
            this._residentialNodeRadioButton = new System.Windows.Forms.RadioButton();
            this._industrialNodeRadioButton = new System.Windows.Forms.RadioButton();
            this._commercialNodeRadioButton = new System.Windows.Forms.RadioButton();
            this._trafficNodeRadioButton = new System.Windows.Forms.RadioButton();
            this._nodeTypeBox.SuspendLayout();
            this._nodeSizeBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // _nodeTypeBox
            // 
            this._nodeTypeBox.Controls.Add(this._residentialNodeRadioButton);
            this._nodeTypeBox.Controls.Add(this._industrialNodeRadioButton);
            this._nodeTypeBox.Controls.Add(this._commercialNodeRadioButton);
            this._nodeTypeBox.Controls.Add(this._trafficNodeRadioButton);
            this._nodeTypeBox.Location = new System.Drawing.Point(12, 12);
            this._nodeTypeBox.Name = "_nodeTypeBox";
            this._nodeTypeBox.Size = new System.Drawing.Size(102, 111);
            this._nodeTypeBox.TabIndex = 0;
            this._nodeTypeBox.TabStop = false;
            this._nodeTypeBox.Text = "Type";
            // 
            // _nodeSizeBox
            // 
            this._nodeSizeBox.Controls.Add(this._smallNodeRadioButton);
            this._nodeSizeBox.Controls.Add(this._normalNodeRadioButton);
            this._nodeSizeBox.Controls.Add(this._largeNodeRadioButton);
            this._nodeSizeBox.Location = new System.Drawing.Point(120, 12);
            this._nodeSizeBox.Name = "_nodeSizeBox";
            this._nodeSizeBox.Size = new System.Drawing.Size(88, 92);
            this._nodeSizeBox.TabIndex = 1;
            this._nodeSizeBox.TabStop = false;
            this._nodeSizeBox.Text = "Size";
            // 
            // _editButton
            // 
            this._editButton.Location = new System.Drawing.Point(12, 131);
            this._editButton.Name = "_editButton";
            this._editButton.Size = new System.Drawing.Size(102, 23);
            this._editButton.TabIndex = 2;
            this._editButton.Text = "Edit node";
            this._editButton.UseVisualStyleBackColor = true;
            this._editButton.Click += new System.EventHandler(this._editButton_Click);
            // 
            // _cancelButton
            // 
            this._cancelButton.Location = new System.Drawing.Point(120, 154);
            this._cancelButton.Name = "_cancelButton";
            this._cancelButton.Size = new System.Drawing.Size(87, 23);
            this._cancelButton.TabIndex = 3;
            this._cancelButton.Text = "Cancel";
            this._cancelButton.UseVisualStyleBackColor = true;
            this._cancelButton.Click += new System.EventHandler(this._cancelButton_Click);
            // 
            // _removeButton
            // 
            this._removeButton.Location = new System.Drawing.Point(120, 131);
            this._removeButton.Name = "_removeButton";
            this._removeButton.Size = new System.Drawing.Size(88, 23);
            this._removeButton.TabIndex = 4;
            this._removeButton.Text = "Remove";
            this._removeButton.UseVisualStyleBackColor = true;
            this._removeButton.Click += new System.EventHandler(this._removeButton_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 154);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(102, 22);
            this.button1.TabIndex = 5;
            this.button1.Text = "Start path";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // _smallNodeRadioButton
            // 
            this._smallNodeRadioButton.AutoSize = true;
            this._smallNodeRadioButton.Location = new System.Drawing.Point(6, 65);
            this._smallNodeRadioButton.Name = "_smallNodeRadioButton";
            this._smallNodeRadioButton.Size = new System.Drawing.Size(50, 17);
            this._smallNodeRadioButton.TabIndex = 2;
            this._smallNodeRadioButton.TabStop = true;
            this._smallNodeRadioButton.Tag = NodeSize.Small;
            this._smallNodeRadioButton.Text = $"Small ({_enviroment.Settings.GetNodeSettings(NodeSize.Small).Capacity})";
            this._smallNodeRadioButton.UseVisualStyleBackColor = true;
            // 
            // _normalNodeRadioButton
            // 
            this._normalNodeRadioButton.AutoSize = true;
            this._normalNodeRadioButton.Checked = true;
            this._normalNodeRadioButton.Location = new System.Drawing.Point(6, 42);
            this._normalNodeRadioButton.Name = "_normalNodeRadioButton";
            this._normalNodeRadioButton.Size = new System.Drawing.Size(58, 17);
            this._normalNodeRadioButton.TabIndex = 1;
            this._normalNodeRadioButton.TabStop = true;
            this._normalNodeRadioButton.Tag = NodeSize.Normal;
            this._normalNodeRadioButton.Text = $"Normal ({_enviroment.Settings.GetNodeSettings(NodeSize.Normal).Capacity})";
            this._normalNodeRadioButton.UseVisualStyleBackColor = true;
            // 
            // _largeNodeRadioButton
            // 
            this._largeNodeRadioButton.AutoSize = true;
            this._largeNodeRadioButton.Location = new System.Drawing.Point(6, 19);
            this._largeNodeRadioButton.Name = "_largeNodeRadioButton";
            this._largeNodeRadioButton.Size = new System.Drawing.Size(52, 17);
            this._largeNodeRadioButton.TabIndex = 0;
            this._largeNodeRadioButton.TabStop = true;
            this._largeNodeRadioButton.Tag = NodeSize.Large;
            this._largeNodeRadioButton.Text = $"Large ({_enviroment.Settings.GetNodeSettings(NodeSize.Large).Capacity})";
            this._largeNodeRadioButton.UseVisualStyleBackColor = true;
            // 
            // _residentialNodeRadioButton
            // 
            this._residentialNodeRadioButton.AutoSize = true;
            this._residentialNodeRadioButton.Checked = true;
            this._residentialNodeRadioButton.Location = new System.Drawing.Point(6, 19);
            this._residentialNodeRadioButton.Name = "_residentialNodeRadioButton";
            this._residentialNodeRadioButton.Size = new System.Drawing.Size(77, 17);
            this._residentialNodeRadioButton.TabIndex = 3;
            this._residentialNodeRadioButton.TabStop = true;
            this._residentialNodeRadioButton.Tag = NodeType.Residential;
            this._residentialNodeRadioButton.Text = "Residential";
            this._residentialNodeRadioButton.UseVisualStyleBackColor = true;
            // 
            // _industrialNodeRadioButton
            // 
            this._industrialNodeRadioButton.AutoSize = true;
            this._industrialNodeRadioButton.Location = new System.Drawing.Point(6, 42);
            this._industrialNodeRadioButton.Name = "_industrialNodeRadioButton";
            this._industrialNodeRadioButton.Size = new System.Drawing.Size(67, 17);
            this._industrialNodeRadioButton.TabIndex = 2;
            this._industrialNodeRadioButton.TabStop = true;
            this._industrialNodeRadioButton.Tag = NodeType.Industrial;
            this._industrialNodeRadioButton.Text = "Industrial";
            this._industrialNodeRadioButton.UseVisualStyleBackColor = true;
            // 
            // _commercialNodeRadioButton
            // 
            this._commercialNodeRadioButton.AutoSize = true;
            this._commercialNodeRadioButton.Location = new System.Drawing.Point(6, 65);
            this._commercialNodeRadioButton.Name = "_commercialNodeRadioButton";
            this._commercialNodeRadioButton.Size = new System.Drawing.Size(79, 17);
            this._commercialNodeRadioButton.TabIndex = 1;
            this._commercialNodeRadioButton.TabStop = true;
            this._commercialNodeRadioButton.Tag = NodeType.Commercial;
            this._commercialNodeRadioButton.Text = "Commercial";
            this._commercialNodeRadioButton.UseVisualStyleBackColor = true;
            // 
            // _trafficNodeRadioButton
            // 
            this._trafficNodeRadioButton.AutoSize = true;
            this._trafficNodeRadioButton.Location = new System.Drawing.Point(6, 88);
            this._trafficNodeRadioButton.Name = "_trafficNodeRadioButton";
            this._trafficNodeRadioButton.Size = new System.Drawing.Size(55, 17);
            this._trafficNodeRadioButton.TabIndex = 0;
            this._trafficNodeRadioButton.TabStop = true;
            this._trafficNodeRadioButton.Tag = NodeType.Traffic;
            this._trafficNodeRadioButton.Text = "Traffic";
            this._trafficNodeRadioButton.UseVisualStyleBackColor = true;
            this._trafficNodeRadioButton.CheckedChanged += new System.EventHandler(this._trafficNodeRadioButton_CheckedChanged);
            // 
            // EditNodeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(220, 189);
            this.Controls.Add(this.button1);
            this.Controls.Add(this._removeButton);
            this.Controls.Add(this._cancelButton);
            this.Controls.Add(this._editButton);
            this.Controls.Add(this._nodeSizeBox);
            this.Controls.Add(this._nodeTypeBox);
            this.Name = "EditNodeForm";
            this.Text = "Edit Node";
            this._nodeTypeBox.ResumeLayout(false);
            this._nodeTypeBox.PerformLayout();
            this._nodeSizeBox.ResumeLayout(false);
            this._nodeSizeBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private GroupBox _nodeTypeBox;
        private RadioButton _industrialNodeRadioButton;
        private RadioButton _commercialNodeRadioButton;
        private RadioButton _trafficNodeRadioButton;
        private RadioButton _residentialNodeRadioButton;
        private GroupBox _nodeSizeBox;
        private RadioButton _smallNodeRadioButton;
        private RadioButton _normalNodeRadioButton;
        private RadioButton _largeNodeRadioButton;
        private Button _editButton;
        private Button _cancelButton;
        private Button _removeButton;
        private Button button1;
    }
}