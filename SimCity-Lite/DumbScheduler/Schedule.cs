﻿using System.Collections.Generic;
using Core;

namespace DumbScheduler
{
    /// <summary>
    /// Class that stores the schedule for an agent
    /// </summary>
    public class Schedule : ISchedule
    {
        /// <summary>
        /// Activities in order of their execution
        /// </summary>
        private LinkedList<ScheduleItem> _items;

        /// <summary>
        /// Creates new Schedule
        /// </summary>
        /// <param name="items">Activities</param>
        public Schedule(LinkedList<ScheduleItem> items)
        {
            _items = items;
        }
        /// <summary>
        /// Getter for first activity in schedule
        /// </summary>
        /// <returns>First item in schedule</returns>
        public ScheduleItem GetCurrentScheduleItem()
        {
            return _items.First.Value;
        }
        /// <summary>
        /// Removes first activity from the schedule
        /// </summary>
        public void MoveSchedule()
        {
            _items.RemoveFirst();
        }
    }
}