﻿using System;

namespace DumbScheduler.Exceptions
{
    /// <summary>
    /// Thrown when there are no residential nodes
    /// </summary>
    public class NoCapacityException : Exception
    {
    }
}