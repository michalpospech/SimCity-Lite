﻿using System;

namespace DumbScheduler.Exceptions
{
    /// <summary>
    /// Thrown when the graph consisting of nodes and links is not strongly connected
    /// </summary>
    public class NotConnectedException : Exception
    {
    }
}