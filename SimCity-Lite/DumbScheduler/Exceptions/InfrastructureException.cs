﻿using System;

namespace DumbScheduler.Exceptions
{
    /// <summary>
    /// Exception thrown when there is not enough commercial or industrial nodes 
    /// </summary>
    public class InfrastructureException:Exception
    {
        
    }
}