﻿using Core;

namespace DumbScheduler
{
    /// <summary>
    /// Simple implementation of the AgentBase class
    /// </summary>
    public class Agent : AgentBase
    {
        /// <summary>
        /// Creates new agent with specified schedule and id
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="schedule">Schedule</param>
        public Agent(int id, ISchedule schedule) : base(id)
        {
            _schedule = schedule;
        }

        /// <summary>
        /// Schedule for the agent
        /// </summary>
        private ISchedule _schedule;

        /// <summary>
        /// Getter for the _schedule field
        /// </summary>
        public override ISchedule Schedule
        {
            get { return _schedule; }
        }
    }
}