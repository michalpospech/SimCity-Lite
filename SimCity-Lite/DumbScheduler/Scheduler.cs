﻿using System;
using System.Collections.Generic;
using Core;
using DumbScheduler.Exceptions;

namespace DumbScheduler
{
    /// <summary>
    /// Class that is used to create agents and schedule their activities 
    /// </summary>
    public class Scheduler : IScheduler
    {
        /// <summary>
        /// Reference to the simulation enviroment
        /// </summary>
        private IEnviroment _enviroment;

        /// <summary>
        /// Creates new instance of the Scheduler
        /// </summary>
        /// <param name="enviroment">Reference to enviroment</param>
        public Scheduler(IEnviroment enviroment)
        {
            _enviroment = enviroment;
        }
        /// <summary>
        /// Checks whether the enviroment is valid and then creates agents with schedules
        /// </summary>
        /// <returns>List of agents with schedules</returns>
        public List<AgentBase> Schedule()
        {
            var connectivityChecker = new ConnectivityChecker(_enviroment);
            if (_enviroment.GetCapacity(NodeType.Residential) == 0)
            {
                throw new NoCapacityException();
            }
            if (!CheckValidity())
            {
                throw new InfrastructureException();
            }
            if (!connectivityChecker.CheckConnectivity())
            {
                throw new NotConnectedException();
            }
            var residential = new List<NodeBase>();
            var commercial = new List<NodeBase>();
            var industrial = new List<NodeBase>();
            var remainingCapacities = new Dictionary<NodeBase, int>();
            foreach (NodeBase node in _enviroment.Nodes)
            {
                switch (node.Type)
                {
                    case NodeType.Commercial:
                        commercial.Add(node);
                        remainingCapacities.Add(node, ((IAgentContainer) node).Capacity);
                        break;
                    case NodeType.Industrial:
                        industrial.Add(node);
                        remainingCapacities.Add(node, ((IAgentContainer) node).Capacity);
                        break;
                    case NodeType.Residential:
                        residential.Add(node);
                        remainingCapacities.Add(node, ((IAgentContainer) node).Capacity);
                        break;
                    case NodeType.Traffic:
                        break;
                }
            }
            int id = 0;
            Pathfinder travelTimes = new Pathfinder(_enviroment);
            Random random = new Random();
            List<AgentBase> agents = new List<AgentBase>();
            foreach (NodeBase residentialNode in residential)
            {
                int capacity = remainingCapacities[residentialNode];
                while (capacity > 0)
                {
                    LinkedList<ScheduleItem> scheduleItems = new LinkedList<ScheduleItem>();
                    //TODO Stuff
                    NodeBase work = industrial[random.Next(industrial.Count)];
                    remainingCapacities[work]--;
                    if (remainingCapacities[work] == 0)
                    {
                        industrial.Remove(work);
                    }

                    NodeBase fun = commercial[random.Next(commercial.Count)];
                    remainingCapacities[fun]--;
                    if (remainingCapacities[fun] == 0)
                    {
                        commercial.Remove(fun);
                    }
                    DateTime start = DateTime.Parse("2017-1-1 6:00:00");

                    TimeSpan timeToWork = TimeSpan.FromSeconds(travelTimes.GetTime(residentialNode, work) * 1.25);
                    TimeSpan timeToFun = TimeSpan.FromSeconds(travelTimes.GetTime(work, fun) * 1.25);
                    TimeSpan timeToHome = TimeSpan.FromSeconds(travelTimes.GetTime(fun, residentialNode) * 1.25);

                    TimeSpan homeTime = TimeSpan.FromMinutes(random.Next(120));
                    DateTime homeDeparture = start.Add(homeTime);
                    scheduleItems.AddLast(new ScheduleItem(residentialNode, homeDeparture, null, homeTime, null));

                    DateTime workEta = homeDeparture.Add(timeToWork);
                    TimeSpan minWorkDuration = new TimeSpan(6, 0, 0);
                    DateTime workEnd = workEta.AddMinutes(random.Next(420, 540));
                    scheduleItems.AddLast(new ScheduleItem(work, workEnd, workEta, minWorkDuration, homeDeparture));

                    DateTime funEta = workEnd.Add(timeToFun);
                    TimeSpan minFunDuration = new TimeSpan(1, 0, 0);
                    DateTime funEnd = funEta.AddMinutes(random.Next(90, 150));
                    scheduleItems.AddLast(new ScheduleItem(fun, funEnd, funEta, minFunDuration, workEnd));

                    DateTime homeEta = funEnd.Add(timeToHome);
                    scheduleItems.AddLast(new ScheduleItem(residentialNode, null, homeEta, null, funEnd));
                    var schedule = new Schedule(scheduleItems);
                    var agent = new Agent(id++, schedule);
                    agents.Add(agent);
                    capacity--;
                    remainingCapacities[residentialNode] = capacity;
                }
            }
            return agents;
        }

        /// <summary>
        /// Checks whether there is enough commercial and industrial nodes for the amount of residential nodes
        /// </summary>
        /// <returns>Result of the check</returns>
        private bool CheckValidity()
        {
            return (_enviroment.GetCapacity(NodeType.Residential) <=
                    _enviroment.GetCapacity(NodeType.Commercial) &&
                    _enviroment.GetCapacity(NodeType.Residential) <=
                    _enviroment.GetCapacity(NodeType.Industrial));
        }
    }
}