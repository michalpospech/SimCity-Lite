﻿using System;
using System.Collections.Generic;
using Core;

namespace DumbScheduler
{
    /// <summary>
    /// Implementaion of Floyd-Warshall algorithm for all-pair shorhtest paths in a graph
    /// </summary>
    public class Pathfinder
    {
        /// <summary>
        /// Reference to enviroment
        /// </summary>
        private readonly IEnviroment _enviroment;

        /// <summary>
        /// Dictionary that stores what index is assigned to which node
        /// </summary>
        private readonly Dictionary<NodeBase, int> _indices;

        /// <summary>
        /// Stores infromation whether the distances have been calculated yet
        /// </summary>
        private bool _calculated;

        /// <summary>
        /// Distances between nodes
        /// </summary>
        private readonly double[,] _distances;
        /// <summary>
        /// Creates new pathfinder using specified enviroment
        /// </summary>
        /// <param name="enviroment">Enviroment</param>
        public Pathfinder(IEnviroment enviroment)
        {
            _enviroment = enviroment;
            _indices = new Dictionary<NodeBase, int>();
            int index = 0;
            foreach (var node in _enviroment.Nodes)
            {
                _indices.Add(node, index++);
            }
            _calculated = false;
            _distances = new double[_enviroment.Nodes.Count, _enviroment.Nodes.Count];
        }

        /// <summary>
        /// Gets the shortest distance between specified nodes
        /// </summary>
        /// <param name="start">Starting node</param>
        /// <param name="end">End node</param>
        /// <returns>Minimal time to get from start to end</returns>
        public double GetTime(NodeBase start, NodeBase end)
        {
            if (!_calculated) 
            {
                for (int index1 = 0; index1 < _enviroment.Nodes.Count; index1++)
                {
                    for (int index2 = 0; index2 < _enviroment.Nodes.Count; index2++)
                    {
                        _distances[index1, index2] = index1 == index2 ? 0 : Double.PositiveInfinity;
                    }
                }
                foreach (LinkBase link in _enviroment.Links)
                {
                    int index1 = _indices[link.Start];
                    int index2 = _indices[link.End];
                    _distances[index1, index2] = link.BaseTravelTime;
                }
                for (int index1 = 0; index1 < _enviroment.Nodes.Count; index1++)
                {
                    for (int index2 = 0; index2 < _enviroment.Nodes.Count; index2++)
                    {
                        for (int index3 = 0; index3 < _enviroment.Nodes.Count; index3++)
                        {
                            _distances[index2, index3] = Math.Min(_distances[index2, index3],
                                _distances[index2, index1] + _distances[index1, index3]);
                        }
                    }
                }
                _calculated = true;
            }
            return _distances[_indices[start], _indices[end]];
        }
    }
}