﻿using System.Collections.Generic;
using System.Linq;
using Core;

namespace DumbScheduler
{
    /// <summary>
    /// Implementation of an algorithm that checks whether the graph is one strongly connected component. The algorithm is based on Kosaraju's algorithm
    /// </summary>
    public class ConnectivityChecker
    {

        /// <summary>
        /// Reference to enviroment
        /// </summary>
        private IEnviroment _enviroment;
        /// <summary>
        /// Time used in DFS algorithm
        /// </summary>
        private int _time;
        /// <summary>
        /// Nodes viseited during second part of algorithm
        /// </summary>
        private HashSet<NodeBase> _visitedTransposed;
        /// <summary>
        /// Dictionary of DFS exit times
        /// </summary>
        private Dictionary<NodeBase, int> _exitTimes;

        /// <summary>
        /// Creates a new instance of the checker
        /// </summary>
        /// <param name="enviroment">Enviroment to evaluate</param>
        public ConnectivityChecker(IEnviroment enviroment)
        {
            _enviroment = enviroment;
            _visitedTransposed = new HashSet<NodeBase>();
            _time = 0;
            _exitTimes = new Dictionary<NodeBase, int>();
        }

        /// <summary>
        /// Checks whether the graph is one strongly connected component
        /// </summary>
        /// <returns>Result of check</returns>
        public bool CheckConnectivity()
        {
            foreach (var node in _enviroment.Nodes)
            {
                Visit(node);
            }
            NodeBase maxNode = _exitTimes.FirstOrDefault(kv => kv.Value == _exitTimes.Values.Max()).Key;
            VisitTransposed(maxNode);
            return _visitedTransposed.Count == _enviroment.Nodes.Count;
        }

        /// <summary>
        /// Recursive method that visits provided node and then all nodes that it points to
        /// </summary>
        /// <param name="node">Node to visit</param>
        private void Visit(NodeBase node)
        {
            if (!_exitTimes.ContainsKey(node))
            {
                _exitTimes.Add(node, -1);
                _time++;
                foreach (var link in node.OutcomingLinks)
                {
                    Visit(link.End);
                }
                _time++;
                _exitTimes[node] = _time;
            }
        }

        /// <summary>
        /// Recursive method that visits provided node and all nodes that point to it
        /// </summary>
        /// <param name="node">Node to visit</param>
        private void VisitTransposed(NodeBase node)
        {
            if (!_visitedTransposed.Contains(node))
            {
                _visitedTransposed.Add(node);
                foreach (var link in node.IncomingLinks)
                {
                    VisitTransposed(link.Start);
                }
            }
        }
    }
}