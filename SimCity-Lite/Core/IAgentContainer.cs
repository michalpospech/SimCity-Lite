﻿using System.Collections.Generic;

namespace Core
{
    /// <summary>
    /// Interface that defines classes that can store Agents
    /// </summary>
    public interface IAgentContainer
    {
        /// <summary>
        /// Getter for container capacity
        /// </summary>
        int Capacity { get; }

        /// <summary>
        /// Getter for agent count
        /// </summary>
        int Count { get; }

        /// <summary>
        /// Getter for agents that are present in container
        /// </summary>
        List<AgentBase> PresentAgents { get; }

        /// <summary>
        /// Adds agent to container
        /// </summary>
        /// <param name="agent">Agent to add</param>
        void AddAgent(AgentBase agent);

        /// <summary>
        /// Empties the container
        /// </summary>
        void Empty();
    }
}