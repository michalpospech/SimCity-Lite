﻿namespace Core
{
    /// <summary>
    /// Base class for representing agents in simulation
    /// </summary>
    public abstract class AgentBase
    {
        /// <summary>
        /// Unique id, uniqueness must be ensured during creation
        /// </summary>
        private int _id;

        /// <summary>
        /// Base contructor that is called by class that inherit this class
        /// </summary>
        /// <param name="id">Id</param>
        protected AgentBase(int id)
        {
            _id = id;
        }

        /// <summary>
        /// Get this agent's schedule
        /// </summary>
        public abstract ISchedule Schedule { get; }
        /// <summary>
        /// Stores current route
        /// </summary>
        private Route _route;

        /// <summary>
        /// Setter for <c>_route</c> field.
        /// </summary>
        public Route Route
        {
            set { _route = value; }
        }

        /// <summary>
        /// Link that is the next to be visiten en route to current target
        /// </summary>
        public LinkBase NextLink
        {
            get { return _route.Next; }
        }

        /// <summary>
        /// Gets number of links remaining in the shortest path to the current target
        /// </summary>
        public int RemainingLinks
        {
            get { return _route.RemainingLinks; }
        }

        /// <summary>
        /// Moves the route plan one link forward
        /// </summary>
        public void SwitchNextLink()
        {
            _route.RemoveFirst();
        }

        /// <summary>
        /// Overrrides the default GetHasCode method, now it returns id.
        /// </summary>
        /// <returns>Hashcode</returns>
        public override int GetHashCode()
        {
            return _id;
        }

        /// <summary>
        /// Overrides the default Equals method. Checks whether obj is of type <c>AgentBase</c>, then compares hash codes.
        /// </summary>
        /// <param name="obj">Object to compare to</param>
        /// <returns>Result of comparism</returns>
        public override bool Equals(object obj)
        {
            if (obj is AgentBase other)
            {
                return other.GetHashCode() == GetHashCode();
            }
            return false;
        }
    }
}