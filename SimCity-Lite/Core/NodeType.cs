﻿namespace Core
{
    /// <summary>
    /// Enum of different node types
    /// </summary>
    public enum NodeType
    {
        /// <summary>
        /// Used by agents as places where they have fun
        /// </summary>
        Commercial,

        /// <summary>
        /// Used by agent as places where they work
        /// </summary>
        Industrial,

        /// <summary>
        /// Used by agents as home
        /// </summary>
        Residential,

        /// <summary>
        /// Used as traffic intersections
        /// </summary>
        Traffic
    }
}