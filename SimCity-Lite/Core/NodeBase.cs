﻿using System.Collections.Generic;
using System.Drawing;

namespace Core
{
    /// <summary>
    /// Base class for all implementations of nodes
    /// </summary>
    public abstract class NodeBase : IDrawable
    {
        /// <summary>
        /// Base constructor that should get called by all implementations of nodes. Uniqueness of id must be ensured during creation.
        /// </summary>
        /// <param name="id">Unique id</param>
        /// <param name="type">Node type</param>
        /// <param name="positionX">X coordinate</param>
        /// <param name="positionY">Y coordinate</param>
        protected NodeBase(int id, NodeType type, int positionX, int positionY)
        {
            _id = id;
            _type = type;
            _positionX = positionX;
            _positionY = positionY;
        }

        /// <summary>
        /// Unique id
        /// </summary>
        private readonly int _id;
        /// <summary>
        /// Node type
        /// </summary>
        private readonly NodeType _type;
        /// <summary>
        /// X position
        /// </summary>
        private readonly int _positionX;
        /// <summary>
        /// Y position
        /// </summary>
        private readonly int _positionY;

        /// <summary>
        /// Gets type of node
        /// </summary>
        public NodeType Type
        {
            get { return _type; }
        }

        /// <summary>
        /// Gets X position
        /// </summary>
        public int PositionX
        {
            get { return _positionX; }
        }

        /// <summary>
        /// Gets Y position
        /// </summary>
        public int PositionY
        {
            get { return _positionY; }
        }

        /// <summary>
        /// Overrides the default GetHashCode method to return id
        /// </summary>
        /// <returns>Unique id</returns>
        public override int GetHashCode()
        {
            return _id;
        }

        /// <summary>
        /// Draws a circle that represents the node
        /// </summary>
        /// <param name="graphics">Graphics to draw on</param>
        public virtual void Draw(Graphics graphics)
        {
            Point coordinates = new Point(_positionX - 7, PositionY - 7);
            var boundingRectangle = new Rectangle(coordinates, new Size(15, 15));
            Color nodeColor = DrawColour;
            using (var brush = new SolidBrush(nodeColor))
            {
                graphics.FillEllipse(brush, boundingRectangle);
            }
        }

        protected Color DrawColour
        {
            get
            {
                Color nodeColor = Color.LightSlateGray;
                switch (_type)
                {
                    case NodeType.Commercial:
                        nodeColor = Color.BlueViolet;
                        break;
                    case NodeType.Industrial:
                        nodeColor = Color.DarkOrange;
                        break;
                    case NodeType.Residential:
                        nodeColor = Color.DodgerBlue;
                        break;
                    case NodeType.Traffic:
                        nodeColor = Color.LightSlateGray;
                        break;
                }

                return nodeColor;
            }
        }

        /// <summary>
        /// Overriden Equals method, objects are equal only if their hashcodes are equal
        /// </summary>
        /// <param name="obj">Object to checkg for equality</param>
        /// <returns>Result of the check</returns>
        public override bool Equals(object obj)
        {
            var other = obj as NodeBase;
            if (other != null)
            {
                return GetHashCode() == other.GetHashCode();
            }
            return false;
        }

        /// <summary>
        /// Abstract getter for links ending in this node
        /// </summary>
        public abstract List<LinkBase> IncomingLinks { get; }

        /// <summary>
        /// Abstract getter for links starting in this node
        /// </summary>
        public abstract List<LinkBase> OutcomingLinks { get; }

        /// <summary>
        /// Adds an incoming ling to this node
        /// </summary>
        /// <param name="link">Link to add</param>
        public abstract void AddIncomingLink(LinkBase link);

        /// <summary>
        /// Removes an incoming link from this node
        /// </summary>
        /// <param name="link">Link to remove</param>
        public abstract void RemoveIncomingLink(LinkBase link);

        /// <summary>
        /// Removes an outcoming link from this node
        /// </summary>
        /// <param name="link">Link to remove</param>
        public abstract void RemoveOutcomingLink(LinkBase link);

        /// <summary>
        /// Adds an outcomig link to this node
        /// </summary>
        /// <param name="link">Link to add</param>
        public abstract void AddOutcomingLink(LinkBase link);

        /// <summary>
        /// Overriden ToString method for easier debiggung
        /// </summary>
        /// <returns>id - type</returns>
        public override string ToString()
        {
            return $"{_id} - {_type}";
        }
    }
}