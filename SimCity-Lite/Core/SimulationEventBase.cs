﻿using System;
using Prism.Events;

namespace Core
{
    /// <summary>
    /// Class that all simulation events in the IEnviroment must inherit from
    /// </summary>
    public abstract class SimulationEventBase : IComparable
    {
        /// <summary>
        /// Time when the event is going to be processe
        /// </summary>
        private DateTime _processTime;

        /// <summary>
        /// Reference to the event aggregator used to propagate new simulation-events to the subscribers
        /// </summary>
        protected readonly IEventAggregator EventAggregator;

        /// <summary>
        /// Creates new simulation-event, is called by all implementations of this class
        /// </summary>
        /// <param name="processTime">Time of processing</param>
        /// <param name="eventAggregator">Reference to the event aggregator</param>
        protected SimulationEventBase(DateTime processTime, IEventAggregator eventAggregator)
        {
            _processTime = processTime;
            EventAggregator = eventAggregator;
        }

        /// <summary>
        /// Getter for _processTime field
        /// </summary>
        public DateTime ProcessTime
        {
            get { return _processTime; }
        }

        /// <summary>
        /// Abstract method that contains the logic for processing of the simulation-event
        /// </summary>
        public abstract void Process();

        /// <summary>
        /// Method that propagates new simulation-event to the event aggregator
        /// </summary>
        /// <param name="newEvent">Simulation-event to propagate</param>
        protected virtual void PropagateNewSimulationEvent(SimulationEventBase newEvent)
        {
            var args = new SimulationEventCreatedArgs(ProcessTime, newEvent);
            EventAggregator.GetEvent<SimulationEventCreated>().Publish(args);
        }

        /// <summary>
        /// Implementation of CompareTo method as defined in the IComparable interface
        /// </summary>
        /// <param name="obj">Obejct to compare with</param>
        /// <returns>Result of comparism</returns>
        public int CompareTo(object obj)
        {
            var otherEvent = obj as SimulationEventBase;
            if (otherEvent != null)
            {
                return _processTime.CompareTo(otherEvent.ProcessTime);
            }
            throw new Exception();
        }
    }
}