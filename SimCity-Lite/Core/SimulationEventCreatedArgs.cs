﻿using System;

namespace Core
{
    /// <summary>
    /// Classed used to store new events and is passed to the event aggregator
    /// </summary>
    public class SimulationEventCreatedArgs
    {
        /// <summary>
        /// Time of creation of the event
        /// </summary>
        private readonly DateTime _creationTime;
        /// <summary>
        /// Newly created event that is to be added to the event calendar
        /// </summary>
        private readonly SimulationEventBase _createdEvent;
        /// <summary>
        /// Creates new instance of this class with specified values
        /// </summary>
        /// <param name="creationTime">Time of creation</param>
        /// <param name="createdEvent">Newly created event</param>
        public SimulationEventCreatedArgs(DateTime creationTime, SimulationEventBase createdEvent)
        {
            _creationTime = creationTime;
            _createdEvent = createdEvent;
        }

        /// <summary>
        /// Getter for _creationTime field
        /// </summary>
        public DateTime CreationTime
        {
            get { return _creationTime; }
        }

        /// <summary>
        /// Getter for _createdEvent field
        /// </summary>
        public SimulationEventBase CreatedEvent
        {
            get { return _createdEvent; }
        }
    }
}