﻿using System.Drawing;

namespace Core
{
    /// <summary>
    /// Interface that specifies that the object can be represented using Graphics
    /// </summary>
    public interface IDrawable
    {
        /// <summary>
        /// Draws a representation of object to provided Graphics
        /// </summary>
        /// <param name="graphics">Graphics to draw on</param>
        void Draw(Graphics graphics);
    }
}