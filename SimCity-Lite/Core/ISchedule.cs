﻿namespace Core
{
    /// <summary>
    /// Inteface for agent schedule. Stores all activities in order of their execution.
    /// </summary>
    public interface ISchedule
    {
        /// <summary>
        /// Accesses the current schedule item
        /// </summary>
        /// <returns>Current schedule item</returns>
        ScheduleItem GetCurrentScheduleItem();

        /// <summary>
        /// Removes first schedule item (usually called after its completion)
        /// </summary>
        void MoveSchedule();
    }
}