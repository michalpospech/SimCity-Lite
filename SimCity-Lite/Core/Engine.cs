﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Prism.Events;

namespace Core
{
    /// <summary>
    /// Class that stores all components of simulation, provides methods for controlling the simulation from the presentation layer.
    /// </summary>
    public class Engine
    {
        /// <summary>
        /// Event aggregator
        /// </summary>
        private readonly IEventAggregator _eventAggregator;

        /// <summary>
        /// Simulation enviroment
        /// </summary>
        private readonly IEnviroment _simulationEnviroment;

        /// <summary>
        /// Scheduler
        /// </summary>
        private readonly IScheduler _scheduler;

        /// <summary>
        /// Event calendar
        /// </summary>
        private SimulationEventCalendar _calendar;

        /// <summary>
        /// List of agents
        /// </summary>
        private List<AgentBase> _agents;

        /// <summary>
        /// Creates new Engine object with injected implementations of IEventAggregator, IScheduler and IEnviroment
        /// </summary>
        /// <param name="eventAggregator">Implementation of IEventAggregator</param>
        /// <param name="simulationEnviroment">Implementation of IEnviroment</param>
        /// <param name="scheduler">Implementation of IScheduler</param>
        public Engine(IEventAggregator eventAggregator, IEnviroment simulationEnviroment, IScheduler scheduler)
        {
            _eventAggregator = eventAggregator;
            _simulationEnviroment = simulationEnviroment;
            _scheduler = scheduler;
            _calendar = new SimulationEventCalendar(_eventAggregator);
        }

        /// <summary>
        /// Getter for _eventAggregator field
        /// </summary>
        public IEventAggregator Aggregator
        {
            get { return _eventAggregator; }
        }

        /// <summary>
        /// Tries to create agents, empties current simulation and initialises it.
        /// </summary>
        public void Start()
        {
            _agents = _scheduler.Schedule();
            Empty();
            _simulationEnviroment.Initialise(_agents, _eventAggregator, _calendar.CurrentTime);
        }

        /// <summary>
        /// Draws current state of simulation to the provided Graphics
        /// </summary>
        /// <param name="graphics">Graphics to draw on</param>
        public void Draw(Graphics graphics)
        {
            foreach (var road in _simulationEnviroment.Links)
            {
                road.Draw(graphics);
            }
            foreach (var node in _simulationEnviroment.Nodes)
            {
                node.Draw(graphics);
            }
        }

        /// <summary>
        /// Getter for current time of simulation
        /// </summary>
        public DateTime CurrentTime
        {
            get { return _calendar.CurrentTime; }
        }

        /// <summary>
        /// Processes next event in the calendar
        /// </summary>
        /// <returns>Number of events remaining in the calendar</returns>
        public int ProcessNext()
        {
            return _calendar.ProcessNextEvent();
        }

        /// <summary>
        /// Empties out whole simulation - deletes all agents and events in calendar
        /// </summary>
        private void Empty()
        {
            foreach (var node in _simulationEnviroment.Nodes)
            {
                var containerNode = node as IAgentContainer;
                containerNode?.Empty();
            }
            foreach (var linkBase in _simulationEnviroment.Links)
            {
                linkBase.Empty();
            }
            _calendar = new SimulationEventCalendar(_eventAggregator);
        }

        /// <summary>
        /// Gets number of events in the calendar
        /// </summary>
        public int EventCount
        {
            get { return _calendar.Count; }
        }
    }
}