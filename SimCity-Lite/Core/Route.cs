﻿using System.Collections.Generic;

namespace Core
{
    /// <summary>
    /// Class that stores sequence of links from start to target
    /// </summary>
    public class Route
    {
        /// <summary>
        /// Sequence of links in order 
        /// </summary>
        private LinkedList<LinkBase> _path;

        /// <summary>
        /// Creates a new instance of this class with specified sequence of links
        /// </summary>
        /// <param name="path">List of links in order</param>
        public Route(LinkedList<LinkBase> path)
        {
            _path = path;
        }

        /// <summary>
        /// Getter for first link in _path, returns null if there is none
        /// </summary>
        public LinkBase Next
        {
            get
            {
                if (_path.Count > 0)
                {
                    return _path.First.Value;
                }
                return null;
            }
        }

        /// <summary>
        /// Getter for count of links
        /// </summary>
        public int RemainingLinks
        {
            get { return _path.Count; }
        }

        /// <summary>
        /// Removes first link from path if there is one
        /// </summary>
        public void RemoveFirst()
        {
            if (_path.Count > 0)
            {
                _path.RemoveFirst();
            }
        }
    }
}