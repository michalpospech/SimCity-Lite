﻿using System;
using Prism.Events;

namespace Core
{
    /// <summary>
    /// Class that stores events to be processed in a priority queue
    /// </summary>
    public class SimulationEventCalendar
    {
        /// <summary>
        /// Stores current time
        /// </summary>
        private DateTime _currentTime;

        /// <summary>
        /// Stores the event aggregator
        /// </summary>
        private readonly IEventAggregator _eventAggregator;

        /// <summary>
        /// Priority queue of events 
        /// </summary>
        private readonly MinHeap<SimulationEventBase> _simulationEventQueue;

        /// <summary>
        /// Public constructor that injects a specified instance of IEventAggregator, subscribes event handler to that instance and sets current time to starting DateTime
        /// </summary>
        /// <param name="eventAggregator">Event aggregator</param>
        public SimulationEventCalendar(IEventAggregator eventAggregator)
        {
            _currentTime = DateTime.Parse("2017-1-1 6:00:00");
            _eventAggregator = eventAggregator;
            _simulationEventQueue = new MinHeap<SimulationEventBase>();
            _eventAggregator.GetEvent<SimulationEventCreated>().Subscribe(EventCreatedHandler);
        }

        /// <summary>
        /// Adds event to the priorirty queue
        /// </summary>
        /// <param name="simulationEvent">Event to add</param>
        private void AddEvent(SimulationEventBase simulationEvent)
        {
            _simulationEventQueue.AddElement(simulationEvent);
        }

        /// <summary>
        /// Method that is subscribed to event aggregator and handles addition of new events to calendar
        /// </summary>
        /// <param name="createdEvent">Newly created event</param>
        private void EventCreatedHandler(SimulationEventCreatedArgs createdEvent)
        {
            AddEvent(createdEvent.CreatedEvent);
        }

        /// <summary>
        /// Processes next event in queue, updates _curentTime and returns the count of remaining events
        /// </summary>
        /// <returns>Number of events remaining in the queue</returns>
        public int ProcessNextEvent()
        {
            var simulationEvent = _simulationEventQueue.GetMin();
            _currentTime = simulationEvent.ProcessTime;
            simulationEvent.Process();
            return _simulationEventQueue.Size;
        }

        /// <summary>
        /// Gets current time of simulation
        /// </summary>
        public DateTime CurrentTime
        {
            get { return _currentTime; }
        }
        /// <summary>
        /// Gets number of events in the calendar
        /// </summary>
        public int Count
        {
            get { return _simulationEventQueue.Size; }
        }
    }
}