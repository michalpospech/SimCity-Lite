﻿using System;
using System.Collections.Generic;
using Prism.Events;

namespace Core
{
    /// <summary>
    /// Interface for the implementation of simulation enviroment
    /// </summary>
    public interface IEnviroment
    {
        /// <summary>
        /// Getter for list of all nodes in the simulation enviroment
        /// </summary>
        List<NodeBase> Nodes { get; }

        /// <summary>
        /// Getter for all links in the simulation enviroment
        /// </summary>
        List<LinkBase> Links { get; }

        /// <summary>
        /// Method that gets total capacity of all nodes of given type
        /// </summary>
        /// <param name="type">Type of node</param>
        /// <returns>Overall capacity</returns>
        int GetCapacity(NodeType type);

        /// <summary>
        /// Method that gets total count of agents in all nodes of given type
        /// </summary>
        /// <param name="type">Type of node</param>
        /// <returns>Number of agents</returns>
        int GetCount(NodeType type);

        /// <summary>
        /// Initialises simulation with given set of agents by adding them to their home nodes and planning their departure
        /// </summary>
        /// <param name="agents">List of used agents</param>
        /// <param name="eventAggregator">Event aggregator used to put events to the event calendar</param>
        /// <param name="initTime">Starting time of the simulation</param>
        void Initialise(List<AgentBase> agents, IEventAggregator eventAggregator, DateTime initTime);
    }
}