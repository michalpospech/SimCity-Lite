﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Core
{
    /// <summary>
    /// Base class for all implementations of links
    /// </summary>
    public abstract class LinkBase : IAgentContainer, IDrawable
    {
        /// <summary>
        /// Reference to the start of the link
        /// </summary>
        private readonly NodeBase _start;

        /// <summary>
        /// Reference to the end of the link
        /// </summary>
        private readonly NodeBase _end;

        /// <summary>
        /// Unique id, uniqueness must be ensured during creation
        /// </summary>
        private readonly int _id;

        /// <summary>
        ///Base constructor that should be called by all implementations of this class 
        /// </summary>
        /// <param name="start">Starting node</param>
        /// <param name="end">Ending node</param>
        /// <param name="id">Unique id</param>
        protected LinkBase(NodeBase start, NodeBase end, int id)
        {
            _start = start;
            _end = end;
            _id = id;
        }

        /// <summary>
        /// Gets the start of link
        /// </summary>
        public NodeBase Start
        {
            get { return _start; }
        }

        /// <summary>
        /// Gets the end of link
        /// </summary>
        public NodeBase End
        {
            get { return _end; }
        }

        /// <summary>
        /// Gets the id of link
        /// </summary>
        public int Id
        {
            get { return _id; }
        }

        /// <summary>
        /// Abstract getter for capacity of the link (as specified in IAgentContainter interface)
        /// </summary>
        public abstract int Capacity { get; }

        /// <summary>
        /// Abstract getter for count of agents in the link (as specified in IAgentContainter interface)
        /// </summary>
        public abstract int Count { get; }

        /// <summary>
        /// Abstract getter for a list of agent in the link (as specified in IAgentContainter interface)
        /// </summary>
        public abstract List<AgentBase> PresentAgents { get; }

        /// <summary>
        /// Abstract getter for base (without consideration of traffic) travel time
        /// </summary>
        public abstract double BaseTravelTime { get; }

        /// <summary>
        /// Getter for length, returns euclidean distance of start and end
        /// </summary>
        protected virtual double Length
        {
            get
            {
                int deltaX = _start.PositionX - _end.PositionX;
                int deltaY = _start.PositionY - _end.PositionY;
                return Math.Sqrt(deltaX * deltaX + deltaY * deltaY);
            }
        }

        /// <summary>
        /// Overriden GetHashCode method, returns _id
        /// </summary>
        /// <returns>Id</returns>
        public override int GetHashCode()
        {
            return _id;
        }

        /// <summary>
        /// Draws a bezier curve as a representation of the link, color depends on ratio of Count/Capacity
        /// </summary>
        /// <param name="graphics">Graphics to draw on</param>
        public virtual void Draw(Graphics graphics)
        {
            Point start = new Point(_start.PositionX, _start.PositionY);
            Point end = new Point(_end.PositionX, _end.PositionY);
            Point offsetPoint = GetOffsetPoint();
            Color linkColor = GetLinkColor();

            using (var p = new Pen(linkColor, 5))
            {
                graphics.DrawBezier(p, start, offsetPoint, offsetPoint, end);
            }
        }

        /// <summary>
        /// Method to calculate color of representation of the link
        /// </summary>
        /// <returns>Color based on Count/Capacity ratio</returns>
        protected virtual Color GetLinkColor()
        {
            var green = Color.ForestGreen;
            var red = Color.DarkRed;
            double ratio = Count / (double) Capacity;
            ratio = ratio > 1 ? 1 : ratio;
            int redChannel = (int) (red.R * ratio + (1 - ratio) * green.R);
            int blueChannel = (int) (red.B * ratio + (1 - ratio) * green.B);
            int greenChannel = (int) (red.G * ratio + (1 - ratio) * green.G);

            Color linkColor = Color.FromArgb(redChannel, greenChannel, blueChannel);
            return linkColor;
        }

        /// <summary>
        /// Method to get the second and thrid point used in drawing a bezier curve
        /// </summary>
        /// <returns>Point</returns>
        protected virtual Point GetOffsetPoint()
        {
            int middleX = (_start.PositionX + _end.PositionX) / 2;
            int middleY = (_start.PositionY + _end.PositionY) / 2;
            int middleVectorX = middleX - _start.PositionX;
            int middleVectorY = middleY - _start.PositionY;
            int temp = -middleVectorY;
            middleVectorY = +middleVectorX;
            middleVectorX = temp;
            double norm = Math.Sqrt(middleVectorX * middleVectorX + middleVectorY * middleVectorY);
            middleVectorX = (int) (middleVectorX / norm * 10);
            middleVectorY = (int) (middleVectorY / norm * 10);
            Point offsetPoint = new Point(middleX + middleVectorX, middleY + middleVectorY);
            return offsetPoint;
        }

        /// <summary>
        /// Overriden Equals method, compares HashCodes
        /// </summary>
        /// <param name="obj">Object to compare to</param>
        /// <returns>Result of comparism</returns>
        public override bool Equals(object obj)
        {
            var other = obj as LinkBase;
            if (other != null)
            {
                return GetHashCode() == other.GetHashCode();
            }
            return false;
        }

        /// <summary>
        /// Abstract method that adds agent to the link
        /// </summary>
        /// <param name="agent">Agent to add</param>
        public abstract void AddAgent(AgentBase agent);
        /// <summary>
        /// Empties out the link
        /// </summary>
        public abstract void Empty();

        /// <summary>
        /// Overriden ToString method, used for easier debugging
        /// </summary>
        /// <returns>(start)-(end)</returns>
        public override string ToString()
        {
            return $"({Start})-({End})";
        }
    }
}