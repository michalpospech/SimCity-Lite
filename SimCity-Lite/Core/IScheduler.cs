﻿using System.Collections.Generic;

namespace Core
{
    /// <summary>
    /// Interface for the scheduler module that gets injected into engine
    /// </summary>
    public interface IScheduler
    {
        /// <summary>
        /// Creates a schedule for agents that will be used in the simulation
        /// </summary>
        /// <returns>List of agents with assigned schedules</returns>
        List<AgentBase> Schedule();
    }
}