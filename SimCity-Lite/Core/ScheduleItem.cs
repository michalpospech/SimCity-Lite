﻿using System;

namespace Core
{
    /// <summary>
    /// Class that stores location, planned start and end and minimal duration of an activity
    /// </summary>
    public class ScheduleItem
    {
        /// <summary>
        /// Location of activity
        /// </summary>
        private readonly NodeBase _target;

        /// <summary>
        /// Expected end of activity, if set to null, it's the last activity
        /// </summary>
        private readonly DateTime? _end;

        /// <summary>
        /// Estiamated time of arrival to target from the previous location, if set to null, it's the first activity
        /// </summary>
        private readonly DateTime? _eta;

        /// <summary>
        /// Estimated start of activity (start of tranport), if set to null, it's to first activity
        /// </summary>
        private readonly DateTime? _start;

        /// <summary>
        /// Minimal duration of activity, if set to null, it's the last activity
        /// </summary>
        private readonly TimeSpan? _minDuration;
        /// <summary>
        /// Constructor that sets all values in to the value of according parameter
        /// </summary>
        /// <param name="target">Place of the activity</param>
        /// <param name="end">End of the activity</param>
        /// <param name="eta">Estimated time of arrival to place of the activity</param>
        /// <param name="minDuration">Minimal duration of the activity</param>
        /// <param name="start">Start of the activity</param>

        public ScheduleItem(NodeBase target, DateTime? end, DateTime? eta, TimeSpan? minDuration, DateTime? start)
        {
            _target = target;
            _end = end;
            _eta = eta;
            _minDuration = minDuration;
            _start = start;
        }

        /// <summary>
        /// Getter for _target field
        /// </summary>
        public NodeBase Target
        {
            get { return _target; }
        }

        /// <summary>
        /// Getter for _end field
        /// </summary>
        public DateTime? End
        {
            get { return _end; }
        }

        /// <summary>
        /// Getter for _start field
        /// </summary>
        public DateTime? Start
        {
            get { return _start; }
        }

        /// <summary>
        /// Getter for _eta property
        /// </summary>
        public DateTime? ETA
        {
            get { return _eta; }
        }

        /// <summary>
        /// Getter for _minDuration field
        /// </summary>
        public TimeSpan? MinDuration
        {
            get { return _minDuration; }
        }
    }
}