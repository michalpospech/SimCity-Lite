﻿using Prism.Events;

namespace Core
{
    /// <summary>
    /// Classed used by event aggregator to identify events
    /// </summary>
    public class SimulationEventCreated : PubSubEvent<SimulationEventCreatedArgs>
    {
    }
}