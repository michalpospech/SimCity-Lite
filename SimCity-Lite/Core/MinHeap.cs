﻿using System;
using System.Collections.Generic;

namespace Core
{
    /// <summary>
    /// Generic binary minimum heap
    /// </summary>
    /// <typeparam name="TObject">Type of object stored in the heap</typeparam>
    public class MinHeap<TObject> where TObject : IComparable

    {
        /// <summary>
        /// Initializes the heap
        /// </summary>
        public MinHeap()
        {
            Elements = new List<TObject>();
        }

        /// <summary>
        /// List that stores the elements of the heap
        /// </summary>
        protected readonly List<TObject> Elements;


        /// <summary>
        /// Method to view the minimal element in the heap without removing it
        /// </summary>
        /// <returns>Minimal element</returns>
        public TObject PeekMin()
        {
            if (Elements.Count > 0)
            {
                return Elements[0];
            }
            return default(TObject);
        }

        /// <summary>
        /// Method to view the minimal element in the heap and removes it
        /// </summary>
        /// <returns>Minimal element</returns>
        public virtual TObject GetMin()
        {
            if (Elements.Count > 0)
            {
                var res = Elements[0];
                SwitchElements(0, Elements.Count - 1);
                Elements.RemoveAt(Elements.Count - 1);
                if (Elements.Count > 1)
                {
                    FixDown(0);
                }

                return res;
            }
            return default(TObject);
        }

        /// <summary>
        /// Switches elements on two provided indices
        /// </summary>
        /// <param name="index1">Index of first element</param>
        /// <param name="index2">Index of second element</param>
        protected virtual void SwitchElements(int index1, int index2)
        {
            TObject temp = Elements[index1];
            Elements[index1] = Elements[index2];
            Elements[index2] = temp;
        }

        /// <summary>
        /// Method that calculates the index of parent of element on provided index
        /// </summary>
        /// <param name="index">Index of child element</param>
        /// <returns>Index of parent element</returns>
        private int GetParentIndex(int index)
        {
            return (index - 1) / 2;
        }
        /// <summary>
        /// Method that calculates index of left child
        /// </summary>
        /// <param name="index">Parent index</param>
        /// <returns>Left child index</returns>
        private int GetLeftChild(int index)
        {
            return index * 2 + 1;
        }
        /// <summary>
        /// Method that calculates index of right child
        /// </summary>
        /// <param name="index">Parent index</param>
        /// <returns>Right child index</returns>
        private int GetRightChild(int index)
        {
            return index * 2 + 2;
        }

        /// <summary>
        /// Fixes possible violation of heap property at specified index by moving it up a layer
        /// </summary>
        /// <param name="index">Index to fix</param>
        protected void FixUp(int index)
        {
            if (index == 0)
            {
                return;
            }

            int parentIndex = GetParentIndex(index);
            TObject currentElement = Elements[index];
            TObject parentElement = Elements[parentIndex];
            if (currentElement.CompareTo(parentElement) < 0)
            {
                SwitchElements(index, parentIndex);
                FixUp(parentIndex);
            }
        }

        /// <summary>
        /// Fixes possible violation of heap property at specified index by moving it down a layer
        /// </summary>
        /// <param name="index">Index to fix</param>
        private void FixDown(int index)
        {
            int leftChildIndex = GetLeftChild(index);
            int rightChildIndex = GetRightChild(index);
            if (leftChildIndex >= Elements.Count)
            {
                return;
            }
            int minChildIndex = leftChildIndex;
            if (rightChildIndex < Elements.Count)
            {
                minChildIndex = Elements[leftChildIndex].CompareTo(Elements[rightChildIndex]) < 0
                    ? leftChildIndex
                    : rightChildIndex;
            }
            if (Elements[index].CompareTo(Elements[minChildIndex]) > 0)
            {
                SwitchElements(index, minChildIndex);
                FixDown(minChildIndex);
            }
        }

        /// <summary>
        /// Adds new element to the heap
        /// </summary>
        /// <param name="element">Element to add</param>
        public virtual void AddElement(TObject element)
        {
            Elements.Add(element);
            FixUp(Elements.Count - 1);
        }

        /// <summary>
        /// Property to access number of items in the Heap
        /// </summary>
        public int Size
        {
            get { return Elements.Count; }
        }
    }
}